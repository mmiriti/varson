unit uopen;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls;

type

  { TfrmOpenCourse }

  TfrmOpenCourse = class(TForm)
    btnOpen: TButton;
    mnuCancel: TButton;
    lbeSWC: TLabeledEdit;
    lbeXML: TLabeledEdit;
    btnBrowseSWC: TSpeedButton;
    btnBrowseXML: TSpeedButton;
    OpenDialog: TOpenDialog;
    procedure btnBrowseSWCClick(Sender: TObject);
    procedure btnBrowseXMLClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure mnuCancelClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmOpenCourse: TfrmOpenCourse;

implementation
uses
  umain;

{$R *.lfm}

{ TfrmOpenCourse }

procedure TfrmOpenCourse.btnBrowseSWCClick(Sender: TObject);
begin
  OpenDialog.Filter := 'SWC|*.swc';
  if OpenDialog.Execute then
  begin
    lbeSWC.Text := OpenDialog.FileName;
  end;
end;

procedure TfrmOpenCourse.btnBrowseXMLClick(Sender: TObject);
begin
  OpenDialog.Filter := 'XML|*.xml';
  if OpenDialog.Execute then
  begin
    lbeXML.Text := OpenDialog.FileName;
  end;
end;

procedure TfrmOpenCourse.btnOpenClick(Sender: TObject);
begin
  frmMain.LoadSWC(lbeSWC.Text);
  frmMain.LoadXML(lbeXML.Text);
  Close;
end;

procedure TfrmOpenCourse.mnuCancelClick(Sender: TObject);
begin
  Close;
end;

end.

