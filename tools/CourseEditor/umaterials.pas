unit umaterials;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XMLRead, DOM, Dialogs, ucommon;

type

  { TMaterialQuizQuestOption }

  TMaterialQuizQuestOption = class
  protected
    FText: string;
    FCorrect: boolean;
    procedure SetText(newText: string);
    procedure SetCorrect(newCorrect: boolean);
  public
    property Text: string read FText write SetText;
    property Correct: boolean read FCorrect write SetCorrect;
    procedure LoadFromDOMNode(domNode: TDOMNode);
    function GetXML(xml: TXMLDocument): TDOMNode;
  end;

  TMaterialQuizQuest = class
  protected
    FTitle: string;
    FImage: string;
    FMultiple: boolean;
    procedure SetTitle(newTitle: string);
    procedure SetImage(newImage: string);
    procedure SetMultiple(newMultiple: boolean);
  public
    Options: array of TMaterialQuizQuestOption;
    property Title: string read FTitle write SetTitle;
    property Image: string read FImage write SetImage;
    property Multiple: boolean read FMultiple write SetMultiple;
    procedure LoadFromDOMNode(domNode: TDOMNode);
    function GetXML(xml: TXMLDocument): TDOMNode;
  end;

  { TMaterialAssignmentAnswer }

  TMaterialAssignmentAnswer = class
  protected
    FText: string;
    FImage: string;
    FCorrect: boolean;
    procedure SetText(newText: string);
    procedure SetImage(newImage: string);
    procedure SetCorrect(newCorrect: boolean);
  public
    property Text: string read FText;
    property Image: string read FImage;
    property Correct: boolean read FCorrect;
    procedure LoadFromDOMNode(domNode: TDOMNode);
    function GetXML(xml: TXMLDocument): TDOMNode;
  end;

  { TMaterialAssignment }

  TMaterialAssignment = class
  private
    procedure SetScenario(AValue: string);
  protected
    FNumber: string;
    FText: string;
    FScenario: string;
    procedure SetNumber(newNumber: string);
    procedure SetText(newText: string);
    procedure SetScenarion(newScenario: string);
  public
    Answers: array of TMaterialAssignmentAnswer;
    property Number: string read FNumber write SetNumber;
    property Text: string read FText write SetText;
    property Scenario: string read FScenario write SetScenario;
    procedure SetScenarioIndex(index: integer);
    function GetScenarioIndex(str: string): integer;
    procedure LoadFromDOMNode(domNode: TDOMNode);
    function GetXML(xml: TXMLDocument): TDOMNode;
  end;

  { TMaterialQuiz }

  TMaterialQuiz = class
  protected
    FNumber: string;
    FName: string;
    FImage: string;
    FMultiple: boolean;
    procedure SetNumber(newNumber: string);
    procedure SetName(newName: string);
    procedure SetMultiple(newMultiple: boolean);
  public
    Quests: array of TMaterialQuizQuest;
    property Number: string read FNumber write SetNumber;
    property Name: string read FName write SetName;
    property Multiple: boolean read FMultiple write SetMultiple;
    procedure LoadFromDOMNode(domNode: TDOMNode);
    function GetXML(xml: TXMLDocument): TDOMNode;
  end;

  { TMaterial }

  TMaterial = class
  protected
    FId: string;
    FName: string;
    FNumber: string;
    procedure SetId(newId: string);
    procedure SetName(newName: string);
    procedure SetNumber(newNumber: string);
  public
    Quizes: array of TMaterialQuiz;
    Assignments: array of TMaterialAssignment;
    property ID: string read FId write SetId;
    property Name: string read FName write SetName;
    property Number: string read FNumber write SetNumber;

    function GetXML(xml: TXMLDocument): TDOMNode;

    constructor Create;
    procedure LoadFromDOMNode(domNode: TDOMNode);
  end;

const
  ScenarioNamesCount: integer = 4;
  ScenarioNames: array[0..4] of string = (
    'Давайте обсудим',
    'Проверь практически',
    'Что нужно сделать дальше?',
    'Какой вариант правильный?',
    'Найди ошибку');

  ScenarioNamesXML: array[0..4] of string = (
    'discuss',
    'check',
    'whatnext',
    'whichcorrect',
    'finderror');


implementation

{ TMaterialAssignmentAnswer }

procedure TMaterialAssignmentAnswer.SetText(newText: string);
begin
  FText := newText;
end;

procedure TMaterialAssignmentAnswer.SetImage(newImage: string);
begin
  FImage := newImage;
end;

procedure TMaterialAssignmentAnswer.SetCorrect(newCorrect: boolean);
begin
  FCorrect := newCorrect;
end;

procedure TMaterialAssignmentAnswer.LoadFromDOMNode(domNode: TDOMNode);
begin

end;

function TMaterialAssignmentAnswer.GetXML(xml: TXMLDocument): TDOMNode;
begin
  Result := xml.CreateElement('answer');
  TDOMElement(Result).SetAttribute('image', FImage);
  TDOMElement(Result).SetAttribute(
    'correct', BoolToStr(FCorrect, 'true', 'false'));
  Result.AppendChild(xml.CreateTextNode(FText));
end;

{ TMaterialAssignment }

procedure TMaterialAssignment.SetScenario(AValue: string);
begin
  if FScenario = AValue then
    Exit;
  FScenario := AValue;
end;

procedure TMaterialAssignment.SetNumber(newNumber: string);
begin
  FNumber := newNumber;
end;

procedure TMaterialAssignment.SetText(newText: string);
begin
  FText := newText;
end;

procedure TMaterialAssignment.SetScenarion(newScenario: string);
begin
  FScenario := newScenario;
end;

procedure TMaterialAssignment.SetScenarioIndex(index: integer);
begin
  FScenario := ScenarioNamesXML[index];
end;

function TMaterialAssignment.GetScenarioIndex(str: string): integer;
var
  i: integer;
begin
  for i := 0 to 4 do
  begin
    if ScenarioNames[i] = str then
    begin
      Result := i;
      Exit;
    end;
  end;
end;

procedure TMaterialAssignment.LoadFromDOMNode(domNode: TDOMNode);
begin

end;

function TMaterialAssignment.GetXML(xml: TXMLDocument): TDOMNode;
var
  i: integer;
begin
  Result := xml.CreateElement('assignment');
  TDOMElement(Result).SetAttribute('number', UTF8Decode(FNumber));
  TDOMElement(Result).SetAttribute('text', UTF8Decode(FText));
  TDOMElement(Result).SetAttribute('scenarion', FScenario);

  for i := 0 to Length(Answers) - 1 do
  begin
    Result.AppendChild(Answers[i].GetXML(xml));
  end;
end;

procedure TMaterialQuizQuest.SetTitle(newTitle: string);
begin
  FTitle := newTitle;
end;

procedure TMaterialQuizQuest.SetImage(newImage: string);
begin
  FImage := newImage;
end;

procedure TMaterialQuizQuest.SetMultiple(newMultiple: boolean);
begin
  FMultiple := newMultiple;
end;

procedure TMaterialQuizQuest.LoadFromDOMNode(domNode: TDOMNode);
var
  newOption: TMaterialQuizQuestOption;
  i: integer;
begin
  for i := 0 to domNode.Attributes.Length - 1 do
  begin
    case domNode.Attributes.Item[i].NodeName of
      'title': FTitle := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
      'image': FImage := domNode.Attributes.Item[i].NodeValue;
    end;
  end;

  for i := 0 to domNode.ChildNodes.Count - 1 do
  begin
    if domNode.ChildNodes.Item[i].NodeName = 'option' then
    begin
      newOption := TMaterialQuizQuestOption.Create;
      newOption.LoadFromDOMNode(domNode.ChildNodes.Item[i]);
      SetLength(Options, Length(Options) + 1);
      Options[Length(Options) - 1] := newOption;
    end;
  end;
end;

function TMaterialQuizQuest.GetXML(xml: TXMLDocument): TDOMNode;
var
  i: integer;
begin
  Result := xml.CreateElement('quest');

  TDOMElement(Result).SetAttribute('title', UTF8Decode(FTitle));
  TDOMElement(Result).SetAttribute('multiple',
    BoolToStr(FMultiple, 'true', 'false'));
  TDOMElement(Result).SetAttribute('image', UTF8Decode(FImage));

  for i := 0 to Length(Options) - 1 do
  begin
    Result.AppendChild(Options[i].GetXML(xml));
  end;
end;

{ TMaterialQuizQuestOption }

procedure TMaterialQuizQuestOption.SetText(newText: string);
begin
  FText := newText;
end;

procedure TMaterialQuizQuestOption.SetCorrect(newCorrect: boolean);
begin
  FCorrect := newCorrect;
end;

procedure TMaterialQuizQuestOption.LoadFromDOMNode(domNode: TDOMNode);
var
  i: integer;
begin
  FText := UTF8Encode(domNode.TextContent);

  for i := 0 to domNode.Attributes.Length - 1 do
  begin
    if domNode.Attributes.Item[i].NodeName = 'correct' then
    begin
      FCorrect := (domNode.Attributes.Item[i].NodeValue = 'true');
    end;
  end;

end;

function TMaterialQuizQuestOption.GetXML(xml: TXMLDocument): TDOMNode;
begin
  Result := xml.CreateElement('option');
  TDOMElement(Result).SetAttribute('correct', BoolToStr(FCorrect, 'true', 'false'));
  Result.AppendChild(xml.CreateTextNode(UTF8Decode(FText)));
end;

{ TMaterialQuiz }

procedure TMaterialQuiz.SetNumber(newNumber: string);
begin
  FNumber := newNumber;
end;

procedure TMaterialQuiz.SetName(newName: string);
begin
  FName := newName;
end;

procedure TMaterialQuiz.SetMultiple(newMultiple: boolean);
begin
  FMultiple := newMultiple;
end;

procedure TMaterialQuiz.LoadFromDOMNode(domNode: TDOMNode);
var
  i: integer;
  newQuest: TMaterialQuizQuest;
begin
  for i := 0 to domNode.Attributes.Length - 1 do
  begin
    case domNode.Attributes.Item[i].NodeName of
      'number': FNumber := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
      'name': FName := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
      'multiple': FMultiple := (domNode.Attributes.Item[i].NodeValue = 'true');
    end;
  end;

  for i := 0 to domNode.ChildNodes.Count - 1 do
  begin
    if domNode.ChildNodes.Item[i].NodeName = 'quest' then
    begin
      newQuest := TMaterialQuizQuest.Create;
      newQuest.LoadFromDOMNode(domNode.ChildNodes.Item[i]);
      SetLength(Quests, Length(Quests) + 1);
      Quests[Length(Quests) - 1] := newQuest;
    end;
  end;
end;

function TMaterialQuiz.GetXML(xml: TXMLDocument): TDOMNode;
var
  i: integer;
begin
  Result := xml.CreateElement('quiz');
  TDOMElement(Result).SetAttribute('number', UTF8Decode(FNumber));
  TDOMElement(Result).SetAttribute('name', UTF8Decode(FName));
  TDOMElement(Result).SetAttribute('multiple', BoolToStr(FMultiple, 'true', 'false'));

  for i := 0 to Length(Quests) - 1 do
  begin
    Result.AppendChild(Quests[i].GetXml(xml));
  end;
end;


{ TMaterial }

procedure TMaterial.SetId(newId: string);
begin
  FId := newId;
end;

procedure TMaterial.SetName(newName: string);
begin
  FName := newName;
end;

procedure TMaterial.SetNumber(newNumber: string);
begin
  FNumber := newNumber;
end;

function TMaterial.GetXML(xml: TXMLDocument): TDOMNode;
var
  i: integer;
begin
  Result := xml.CreateElement('material');
  TDOMElement(Result).SetAttribute('id', UTF8Decode(FId));
  TDOMElement(Result).SetAttribute('name', UTF8Decode(FName));
  TDOMElement(Result).SetAttribute('number', UTF8Decode(FNumber));

  for i := 0 to Length(Quizes) - 1 do
  begin
    Result.AppendChild(Quizes[i].GetXML(xml));
  end;

  for i := 0 to Length(Assignments) - 1 do
  begin
    Result.AppendChild(Assignments[i].GetXML(xml));
  end;

end;

constructor TMaterial.Create;
begin

end;

procedure TMaterial.LoadFromDOMNode(domNode: TDOMNode);
var
  i: integer;
  newQuiz: TMaterialQuiz;
  newAssignment: TMaterialAssignment;
begin
  for i := 0 to domNode.Attributes.Length - 1 do
  begin
    case domNode.Attributes.Item[i].NodeName of
      'name': FName := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
      'number': FNumber := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
      'id': FId := UTF8Encode(domNode.Attributes.Item[i].NodeValue);
    end;
  end;

  for i := 0 to domNode.ChildNodes.Count - 1 do
  begin
    if domNode.ChildNodes.Item[i].NodeName = 'quiz' then
    begin
      newQuiz := TMaterialQuiz.Create;
      newQuiz.LoadFromDOMNode(domNode.ChildNodes.Item[i]);
      SetLength(Quizes, Length(Quizes) + 1);
      Quizes[Length(Quizes) - 1] := newQuiz;
    end
    else
    if domNode.ChildNodes.Item[i].NodeName = 'assignment' then
    begin
      newAssignment := TMaterialAssignment.Create;
      newAssignment.LoadFromDOMNode(domNode.ChildNodes.Item[i]);
      SetLength(Assignments, Length(Assignments) + 1);
      Assignments[Length(Assignments) - 1] := newAssignment;
    end;
  end;
end;

end.
