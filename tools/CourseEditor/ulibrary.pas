unit ulibrary;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Menus, XMLRead, XMLWrite, DOM, ulog;

type

  { TfrmLibrary }

  TfrmLibrary = class(TForm)
    lbxLib: TListBox;
    mnuCreate: TMenuItem;
    mnuLib: TPopupMenu;
    procedure mnuCreateClick(Sender: TObject);
    procedure pnlNotLoadedClick(Sender: TObject);
  private
    { private declarations }
  public
    procedure LoadXML(xml: TXMLDocument);
  end;

var
  frmLibrary: TfrmLibrary;

implementation

uses
  uMain, umaterials;

{$R *.lfm}

{ TfrmLibrary }

procedure TfrmLibrary.mnuCreateClick(Sender: TObject);
var
  i, num: integer;
  newMaterial: TMaterial;
begin
  num := 1;
  for i := 0 to lbxLib.Count - 1 do
  begin
    if lbxLib.Selected[i] then
    begin
      newMaterial := TMaterial.Create;
      newMaterial.ID := lbxLib.Items.Strings[i];
      newMaterial.Name := 'Безымянная таблица';
      newMaterial.Number := IntToStr(num);
      Inc(num);
      SetLength(frmMain.Materials, Length(frmMain.Materials) + 1);
      frmMain.Materials[Length(frmMain.Materials) - 1] := newMaterial;
    end;
  end;
  frmMain.RefreshMaterialsList;
end;

procedure TfrmLibrary.pnlNotLoadedClick(Sender: TObject);
begin
  frmMain.mnuOpenClick(Sender);
end;

procedure TfrmLibrary.LoadXML(xml: TXMLDocument);
var
  domNode, libNode: TDOMNode;
  i, j: integer;
  itemName, itemType: string;
begin
  lbxLib.Clear;

  domNode := xml.DocumentElement.FindNode('libraries');

  for i := 0 to domNode.FirstChild.ChildNodes.Count - 1 do
  begin
    libNode := domNode.FirstChild.ChildNodes.Item[i];

    for j := 0 to libNode.Attributes.Length - 1 do
    begin
      if libNode.Attributes.Item[j].NodeName = 'name' then
      begin
        lbxLib.Items.Add(libNode.Attributes.Item[j].NodeValue);
      end;
    end;
  end;
end;

end.
