unit uquestedit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, ExtCtrls, ComCtrls, umaterials;

type

  { TfrmQuestEditor }

  TfrmQuestEditor = class(TForm)
    btnSave: TButton;
    cbxClass: TComboBox;
    cbxMultiple: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lbxOptions: TListBox;
    memQuestText: TMemo;
    mnuAbvg: TMenuItem;
    mnuEdit: TMenuItem;
    mnuCorrect: TMenuItem;
    mnuDelete: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    PopupMenuTemplates: TPopupMenu;
    PopupMenuOptions: TPopupMenu;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    btnAddOption: TToolButton;
    procedure btnAddOptionClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbxOptionsDblClick(Sender: TObject);
    procedure mnuAbvgClick(Sender: TObject);
    procedure mnuCorrectClick(Sender: TObject);
    procedure mnuDeleteClick(Sender: TObject);
    procedure mnuEditClick(Sender: TObject);
    procedure PopupMenuOptionsPopup(Sender: TObject);
  private
    currentQuest: TMaterialQuizQuest;
  public
    procedure EditQuest(quest: TMaterialQuizQuest);
    procedure RefreshOptionsList;
  end;

var
  frmQuestEditor: TfrmQuestEditor;

implementation

uses
  uoptionedit, ulibrary, uquizeditor;

{$R *.lfm}

{ TfrmQuestEditor }

procedure TfrmQuestEditor.FormCreate(Sender: TObject);
begin

end;

procedure TfrmQuestEditor.btnSaveClick(Sender: TObject);
begin
  currentQuest.Title := memQuestText.Text;
  currentQuest.Image := cbxClass.Text;
  currentQuest.Multiple := cbxMultiple.Checked;
  frmQuizEditor.RefreshQuestList;
  Close;
end;

procedure TfrmQuestEditor.btnAddOptionClick(Sender: TObject);
var
  newOption: TMaterialQuizQuestOption;
begin
  newOption := TMaterialQuizQuestOption.Create;
  frmOptionEditor.EditOption(newOption);

  SetLength(currentQuest.Options, Length(currentQuest.Options) + 1);
  currentQuest.Options[Length(currentQuest.Options) - 1] := newOption;
end;

procedure TfrmQuestEditor.lbxOptionsDblClick(Sender: TObject);
begin
  if lbxOptions.ItemIndex <> -1 then
  begin
    frmOptionEditor.EditOption(currentQuest.Options[lbxOptions.ItemIndex]);
  end;
end;

procedure TfrmQuestEditor.mnuAbvgClick(Sender: TObject);
const
  opts: array[0..3] of string = ('а', 'б', 'в', 'г');
var
  i: integer;
begin
  if Length(currentQuest.Options) > 0 then
  begin
    if MessageDlg('Осторожно!',
      'Все существующие варианты ответов будут удалены!',
      mtWarning, mbOKCancel, 0) = mrCancel then
      Exit;
  end;

  SetLength(currentQuest.Options, 0);

  for i := 0 to 3 do
  begin
    SetLength(currentQuest.Options, Length(currentQuest.Options) + 1);
    currentQuest.Options[Length(currentQuest.Options) - 1] :=
      TMaterialQuizQuestOption.Create;
    currentQuest.Options[Length(currentQuest.Options) - 1].Text := opts[i];
  end;

  RefreshOptionsList;

end;

procedure TfrmQuestEditor.mnuCorrectClick(Sender: TObject);
begin
  if lbxOptions.ItemIndex <> -1 then
  begin
    (Sender as TMenuItem).Checked := not (Sender as TMenuItem).Checked;
    currentQuest.Options[lbxOptions.ItemIndex].Correct := (Sender as TMenuItem).Checked;
    RefreshOptionsList;
  end;
end;

procedure TfrmQuestEditor.mnuDeleteClick(Sender: TObject);
var
  i, k: integer;
begin
  i := lbxOptions.ItemIndex;

  if i <> -1 then
  begin
    for k := i to Length(currentQuest.Options) - 2 do
      currentQuest.Options[i] := currentQuest.Options[i + 1];
    SetLength(currentQuest.Options, Length(currentQuest.Options) - 1);
    RefreshOptionsList;
  end;
end;

procedure TfrmQuestEditor.mnuEditClick(Sender: TObject);
begin
  lbxOptionsDblClick(Sender);
end;

procedure TfrmQuestEditor.PopupMenuOptionsPopup(Sender: TObject);
var
  i: integer;
begin

  i := lbxOptions.ItemIndex;

  if i <> -1 then
  begin
    mnuCorrect.Enabled := True;
    mnuCorrect.Checked := currentQuest.Options[i].Correct;
    mnuEdit.Enabled := True;
    mnuDelete.Enabled := True;
  end
  else
  begin
    mnuCorrect.Enabled := False;
    mnuEdit.Enabled := False;
    mnuDelete.Enabled := False;
  end;
end;

procedure TfrmQuestEditor.EditQuest(quest: TMaterialQuizQuest);
begin
  currentQuest := quest;

  cbxClass.Items := frmLibrary.lbxLib.Items;
  memQuestText.Text := quest.Title;
  cbxClass.Text := quest.Image;
  cbxMultiple.Checked := quest.Multiple;

  RefreshOptionsList;

  Show;
end;

procedure TfrmQuestEditor.RefreshOptionsList;
var
  i: integer;
  optStr: string;
begin
  lbxOptions.Clear;
  for i := 0 to Length(currentQuest.Options) - 1 do
  begin
    optStr := '[';
    if currentQuest.Options[i].Correct then
    begin
      optStr := optStr + 'x';
    end
    else
      optStr := optStr + ' ';

    optStr := optStr + '] ' + currentQuest.Options[i].Text;

    lbxOptions.Items.Add(optStr);
  end;
end;

end.
