{%RunWorkingDir C:\Users\Michael\Google Диск\WORK\Варсон\application\release\CourseEditor}
unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ExtCtrls, StdCtrls, ComCtrls, XMLRead, XMLWrite, DOM, umaterials, Process,
  ulog, ulibrary, umaterialedit, uopen, ShellApi, Windows, LCLIntf, uabout;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    btnAddTable: TButton;
    MainImageList: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    lvlMaterials: TListView;
    MenuItem2: TMenuItem;
    mnuAbout: TMenuItem;
    mnuDoc: TMenuItem;
    mnuHelp: TMenuItem;
    mnuSaveXMLAs: TMenuItem;
    mnuOpen: TMenuItem;
    mnuDelete: TMenuItem;
    mnuEdit: TMenuItem;
    mnuLog: TMenuItem;
    mnuSaveXML: TMenuItem;
    OpenDialog: TOpenDialog;
    mnuPopMain: TPopupMenu;
    SaveDialog: TSaveDialog;
    StatusBar: TStatusBar;
    txtSubtitle: TEdit;
    txtTitle: TEdit;
    MenuItem1: TMenuItem;
    mnuLibrary: TMenuItem;
    mnuTools: TMenuItem;
    mnuExit: TMenuItem;
    mnuLoadSWC: TMenuItem;
    mnuOpenXML: TMenuItem;
    mnuFile: TMenuItem;
    mnuMain: TMainMenu;
    pnlTop: TPanel;
    procedure btnAddTableClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvlMaterialsDblClick(Sender: TObject);
    procedure lvlMaterialsKeyDown(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure mnuAboutClick(Sender: TObject);
    procedure mnuDeleteClick(Sender: TObject);
    procedure mnuDocClick(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure mnuLibraryClick(Sender: TObject);
    procedure mnuLoadSWCClick(Sender: TObject);
    procedure mnuLogClick(Sender: TObject);
    procedure mnuNewMaterialClick(Sender: TObject);
    procedure mnuOpenClick(Sender: TObject);
    procedure mnuOpenXMLClick(Sender: TObject);
    procedure mnuSaveXMLAsClick(Sender: TObject);
    procedure mnuSaveXMLClick(Sender: TObject);
  private
    function generalInfoNode(xml: TXMLDocument): TDOMNode;
    function materialListNode(xml: TXMLDocument): TDOMNode;
  public
    xmlFileName: string;
    swcFileName: string;
    Materials: array of TMaterial;
    procedure RefreshMaterialsList;
    procedure LoadXML(FileName: string);
    procedure SaveXML(FileName: string);
    procedure LoadSWC(FileName: string);
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.mnuExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmMain.btnAddTableClick(Sender: TObject);
var
  newMaterial: TMaterial;
begin
  newMaterial := TMaterial.Create;
  newMaterial.Number := '0';
  newMaterial.Name := 'Безымянная таблица';
  SetLength(Materials, Length(Materials) + 1);
  Materials[Length(Materials) - 1] := newMaterial;
  RefreshMaterialsList;
  frmMaterialEditor.editMaterial(newMaterial);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
end;

procedure TfrmMain.lvlMaterialsDblClick(Sender: TObject);
begin
  if lvlMaterials.ItemIndex <> -1 then
  begin
    frmMaterialEditor.editMaterial(Materials[lvlMaterials.ItemIndex]);
  end;
end;

procedure TfrmMain.lvlMaterialsKeyDown(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
end;

procedure TfrmMain.mnuAboutClick(Sender: TObject);
begin
  frmAbout.ShowModal;
end;

procedure TfrmMain.mnuDeleteClick(Sender: TObject);
var
  i: integer;
begin
  if lvlMaterials.ItemIndex <> -1 then
  begin
    for i := lvlMaterials.ItemIndex to Length(Materials) - 2 do
    begin
      Materials[i] := Materials[i + 1];
    end;

    SetLength(Materials, Length(Materials) - 1);
    RefreshMaterialsList;
  end;
end;

procedure TfrmMain.mnuDocClick(Sender: TObject);
begin
  OpenURL('https://docs.google.com/document/d/1Mg1kq18jHZ4ITYnXdnqhTkeqg2wHtqVUG7J9FJUBwX8/edit?usp=sharing');
end;

procedure TfrmMain.mnuLibraryClick(Sender: TObject);
begin
  frmLibrary.Show;
end;

procedure TfrmMain.mnuLoadSWCClick(Sender: TObject);
begin
  OpenDialog.Filter := 'SWC|*.swc';
  if OpenDialog.Execute then
  begin
    LoadSWC(OpenDialog.FileName);
  end;
end;

procedure TfrmMain.mnuLogClick(Sender: TObject);
begin
  frmLog.Show;
end;

procedure TfrmMain.mnuNewMaterialClick(Sender: TObject);
begin

end;

procedure TfrmMain.mnuOpenClick(Sender: TObject);
begin
  frmOpenCourse.ShowModal;
end;

procedure TfrmMain.mnuOpenXMLClick(Sender: TObject);
var
  xmlDocument: TXMLDocument;
  xmlChild: TDOMNode;
  i: integer;
begin
  OpenDialog.Filter := 'XML|*.xml';
  if OpenDialog.Execute then
  begin
    LoadXML(OpenDialog.FileName);
  end;
end;

{***
 Главное меню: сохранить XML как...
 ***}
procedure TfrmMain.mnuSaveXMLAsClick(Sender: TObject);
begin
  if SaveDialog.Execute then
  begin
    SaveXML(UTF8Decode(SaveDialog.FileName));
  end;
end;

{***
 Главное меню: сохранение XML
 ***}
procedure TfrmMain.mnuSaveXMLClick(Sender: TObject);
begin
  if xmlFileName <> '' then
  begin
    SaveXML(xmlFileName);
  end
  else
    mnuSaveXMLAsClick(Sender);
end;

{***
 Генерация раздела XML с основной информацией
 ***}
function TfrmMain.generalInfoNode(xml: TXMLDocument): TDOMNode;
var
  tmpNode: TDOMNode;
begin
  Result := xml.CreateElement('generalInfo');

  tmpNode := xml.CreateElement('name');
  tmpNode.AppendChild(xml.CreateTextNode(UTF8Decode(txtTitle.Text)));
  Result.AppendChild(tmpNode);

  tmpNode := xml.CreateElement('subtitle');
  tmpNode.AppendChild(xml.CreateTextNode(UTF8Decode(txtSubtitle.Text)));
  Result.AppendChild(tmpNode);
end;

{***
 Генерация XML со списком материалов
 ***}
function TfrmMain.materialListNode(xml: TXMLDocument): TDOMNode;
var
  i: integer;
begin
  Result := xml.CreateElement('materialsList');

  for i := 0 to Length(Materials) - 1 do
  begin
    Result.AppendChild(Materials[i].GetXML(xml));
  end;
end;

{***
 Обновить список материалов
 ***}
procedure TfrmMain.RefreshMaterialsList;
var
  i: integer;
  newItem: TListItem;
begin
  lvlMaterials.Clear;

  for i := 0 to Length(Materials) - 1 do
  begin
    newItem := TListItem.Create(lvlMaterials.Items);
    newItem.Caption := Materials[i].Number + '. ' + Materials[i].Name +
      ' (' + Materials[i].ID + ')';
    newItem.ImageIndex := 0;
    lvlMaterials.Items.AddItem(newItem);
  end;
end;

{***
 Загрузка XML
 ***}
procedure TfrmMain.LoadXML(FileName: string);
var
  xmlDocument: TXMLDocument;
  xmlChild: TDOMNode;
  i: integer;
begin
  ReadXMLFile(xmlDocument, UTF8Decode(FileName));

  // General info
  xmlChild := xmlDocument.DocumentElement.FindNode('generalInfo');
  txtTitle.Text := UTF8Encode(xmlChild.FindNode('name').FirstChild.NodeValue);
  txtSubtitle.Text := UTF8Encode(xmlChild.FindNode('subtitle').FirstChild.NodeValue);

  // Material list
  xmlChild := xmlDocument.DocumentElement.FindNode('materialsList');

  // Free old materials
  for i := 0 to Length(Materials) - 1 do
  begin
    Materials[i].Free;
  end;

  SetLength(Materials, xmlChild.ChildNodes.Count);

  frmLog.Log('Количество таблиц: ' + IntToStr(Length(Materials)));

  // Load materials from XML
  for i := 0 to xmlChild.ChildNodes.Count - 1 do
  begin
    Materials[i] := TMaterial.Create;
    Materials[i].LoadFromDOMNode(xmlChild.ChildNodes.Item[i]);
  end;

  // Refresh list
  RefreshMaterialsList;

  StatusBar.Panels[0].Text := FileName;
  xmlFileName := FileName;
end;

{***
 Сохранение XML
 ***}
procedure TfrmMain.SaveXML(FileName: string);
var
  newXml: TXMLDocument;
  rootNode, currentNode: TDOMNode;
begin
  try
    newXml := TXMLDocument.Create;

    rootNode := newXml.CreateElement('data');
    newXml.AppendChild(rootNode);

    rootNode := newXml.DocumentElement;
    rootNode.AppendChild(generalInfoNode(newXml));
    rootNode.AppendChild(materialListNode(newXml));


    WriteXMLFile(newXml, UTF8Decode(FileName));
    ShowMessage('XML сохранен');
  except
    on E: Exception do
    begin
      MessageDlg('Ошибка!', 'Не удалось сохранить файл!' +
        #13#10#13#10 + E.Message,
        mtError, [mbClose], 0);
      frmLog.Log(E.ClassName + ': ' + E.Message);
      frmLog.Show;
    end;
  end;
end;

{***
Загрузка SWC
***}
procedure TfrmMain.LoadSWC(FileName: string);
var
  CMD: string;
  SevenZipProcess: TProcess;
  CatalogXML: TXMLDocument;
begin
  DeleteFile('catalog.xml');
  DeleteFile('library.swf');

  if not FileExists('7z.exe') then
  begin
    frmLog.Log(
      'Ошибка: не обнаружен файл распаковщика 7z.exe. Возможно не все файлы были перенесены корректно');
    frmLog.Show;
    Exit;
  end;

  SevenZipProcess := TProcess.Create(nil);
  CMD := '7z e "' + UTF8Decode(FileName) + '" *.* -y';
  frmLog.Log('Выполнение команды: ' + CMD);
  SevenZipProcess.CommandLine := CMD;
  SevenZipProcess.Options := SevenZipProcess.Options + [poNoConsole, poWaitOnExit];
  SevenZipProcess.Execute;
  SevenZipProcess.Free;

  swcFileName := FileName;

  if FileExists('catalog.xml') then
  begin
    ReadXMLFile(CatalogXML, 'catalog.xml');
    frmLibrary.LoadXML(CatalogXML);
    frmLibrary.Show;
    StatusBar.Panels[1].Text := FileName;
    DeleteFile('catalog.xml');
    DeleteFile('library.swf');
  end
  else
  begin
    frmLog.Log(
      'После распаковки файла .swc не обнаружен файл catalog.xml. Возможно распаковка прошла с ошибками либо .swc файл поврежден');
    frmLog.Show;
  end;
end;

end.
