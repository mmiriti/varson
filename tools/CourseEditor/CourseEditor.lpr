program CourseEditor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, uMain, umaterials, ucommon, ulog, ulibrary, umaterialedit, uopen,
  uquizeditor, uoptionedit, uquestedit, uassigneditor, uassignansweredit,
uabout;

{$R *.res}

begin
  Application.Title:='Редактор курсов';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmLog, frmLog);
  Application.CreateForm(TfrmLibrary, frmLibrary);
  Application.CreateForm(TfrmMaterialEditor, frmMaterialEditor);
  Application.CreateForm(TfrmOpenCourse, frmOpenCourse);
  Application.CreateForm(TfrmQuizEditor, frmQuizEditor);
  Application.CreateForm(TfrmOptionEditor, frmOptionEditor);
  Application.CreateForm(TfrmQuestEditor, frmQuestEditor);
  Application.CreateForm(TfrmAssignmentsEditor, frmAssignmentsEditor);
  Application.CreateForm(TfrmAssignmentAnswerEditor, frmAssignmentAnswerEditor);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.Run;
end.

