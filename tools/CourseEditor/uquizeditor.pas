unit uquizeditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, umaterials;

type

  { TfrmQuizEditor }

  TfrmQuizEditor = class(TForm)
    btnSave: TButton;
    btnNewQuest: TButton;
    Label1: TLabel;
    lbeNumber: TLabeledEdit;
    lbeName: TLabeledEdit;
    lbxOptions: TListBox;
    procedure btnNewQuestClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lbxOptionsDblClick(Sender: TObject);
  private
    currentQuiz: TMaterialQuiz;
  public
    procedure EditQuiz(quiz: TMaterialQuiz);
    procedure RefreshQuestList;
  end;

var
  frmQuizEditor: TfrmQuizEditor;

implementation

uses
  uquestedit, ulibrary, umaterialedit;

{$R *.lfm}

{ TfrmQuizEditor }

procedure TfrmQuizEditor.FormCreate(Sender: TObject);
begin

end;

procedure TfrmQuizEditor.btnSaveClick(Sender: TObject);
begin
  currentQuiz.Number := lbeNumber.Text;
  currentQuiz.Name := lbeName.Text;
  Close;
  frmMaterialEditor.RefreshQuizList;
end;

procedure TfrmQuizEditor.btnNewQuestClick(Sender: TObject);
var
  newQuest: TMaterialQuizQuest;
begin
  newQuest := TMaterialQuizQuest.Create;
  newQuest.Multiple := True;
  frmQuestEditor.EditQuest(newQuest);

  SetLength(currentQuiz.Quests, Length(currentQuiz.Quests) + 1);
  currentQuiz.Quests[Length(currentQuiz.Quests) - 1] := newQuest;
end;

procedure TfrmQuizEditor.lbxOptionsDblClick(Sender: TObject);
begin
  if lbxOptions.ItemIndex <> -1 then
  begin
    frmQuestEditor.EditQuest(currentQuiz.Quests[lbxOptions.ItemIndex]);
  end;
end;

procedure TfrmQuizEditor.EditQuiz(quiz: TMaterialQuiz);
begin
  currentQuiz := quiz;
  lbeNumber.Text := quiz.Number;
  lbeName.Text := quiz.Name;

  RefreshQuestList;

  Show;
end;

procedure TfrmQuizEditor.RefreshQuestList;
var
  i: integer;
begin
  lbxOptions.Clear;

  for i := 0 to Length(currentQuiz.Quests) - 1 do
  begin
    lbxOptions.Items.Add(currentQuiz.Quests[i].Title);
  end;
end;

end.
