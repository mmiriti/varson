unit uoptionedit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, umaterials;

type

  { TfrmOptionEditor }

  TfrmOptionEditor = class(TForm)
    btnSave: TButton;
    chxCorrect: TCheckBox;
    memText: TMemo;
    procedure btnSaveClick(Sender: TObject);
  private
    currentOpt: TMaterialQuizQuestOption;
  public
    procedure EditOption(opt: TMaterialQuizQuestOption);
  end;

var
  frmOptionEditor: TfrmOptionEditor;

implementation

uses
  uquestedit;

{$R *.lfm}

{ TfrmOptionEditor }

procedure TfrmOptionEditor.btnSaveClick(Sender: TObject);
begin
  currentOpt.Text := memText.Text;
  currentOpt.Correct := chxCorrect.Checked;
  Close;
  frmQuestEditor.RefreshOptionsList;
end;

procedure TfrmOptionEditor.EditOption(opt: TMaterialQuizQuestOption);
begin
  currentOpt := opt;
  memText.Text := opt.Text;
  chxCorrect.Checked := opt.Correct;
  Show;
end;

end.
