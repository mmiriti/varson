unit ulog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TfrmLog }

  TfrmLog = class(TForm)
    memLog: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    procedure Log(S: string);
  end;

var
  frmLog: TfrmLog;

implementation

{$R *.lfm}

{ TfrmLog }

procedure TfrmLog.FormCreate(Sender: TObject);
begin

end;

procedure TfrmLog.Log(S: string);
begin
  memLog.Lines.Add('[' + DateTimeToStr(Now) + ']: ' + S);
end;

end.

