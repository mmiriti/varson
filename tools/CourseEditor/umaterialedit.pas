unit umaterialedit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Menus, ComCtrls, umaterials;

type

  { TfrmMaterialEditor }

  TfrmMaterialEditor = class(TForm)
    btnSaveAndExit: TButton;
    cbxClass: TComboBox;
    lblClass: TLabel;
    lbeNumber: TLabeledEdit;
    lbeName: TLabeledEdit;
    lbxQuizes: TListBox;
    mnuDelete: TMenuItem;
    PageControl1: TPageControl;
    Panel1: TPanel;
    QuizPopupMenu: TPopupMenu;
    tabTests: TTabSheet;
    ToolBar1: TToolBar;
    btnAddTest: TToolButton;
    procedure btnAddAssignmntClick(Sender: TObject);
    procedure btnSaveAndExitClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCreateQuizClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbxQuizesDblClick(Sender: TObject);
    procedure mnuDeleteClick(Sender: TObject);
    procedure btnAddTestClick(Sender: TObject);
  private
    currentMaterial: TMaterial;
  public
    procedure editMaterial(mat: TMaterial);
    procedure RefreshQuizList;
  end;

var
  frmMaterialEditor: TfrmMaterialEditor;

implementation

uses
  ulibrary, uMain, uquizeditor, uassigneditor;

{$R *.lfm}

{ Some fucking change }

{ TfrmMaterialEditor }

procedure TfrmMaterialEditor.FormShow(Sender: TObject);
begin
  cbxClass.Items := frmLibrary.lbxLib.Items;
end;

procedure TfrmMaterialEditor.lbxQuizesDblClick(Sender: TObject);
begin
  if lbxQuizes.ItemIndex <> -1 then
  begin
    frmQuizEditor.EditQuiz(currentMaterial.Quizes[lbxQuizes.ItemIndex]);
  end;
end;

procedure TfrmMaterialEditor.mnuDeleteClick(Sender: TObject);
var
  i: integer;
begin
  if lbxQuizes.ItemIndex <> -1 then
  begin
    for i := lbxQuizes.ItemIndex to Length(currentMaterial.Quizes) - 2 do
    begin
      currentMaterial.Quizes[i] := currentMaterial.Quizes[i + 1];
    end;

    SetLength(currentMaterial.Quizes, Length(currentMaterial.Quizes) - 1);
    RefreshQuizList;
  end;
end;

procedure TfrmMaterialEditor.btnAddTestClick(Sender: TObject);
var
  newQuiz: TMaterialQuiz;
begin
  newQuiz := TMaterialQuiz.Create;
  frmQuizEditor.EditQuiz(newQuiz);
  SetLength(currentMaterial.Quizes, Length(currentMaterial.Quizes) + 1);
  currentMaterial.Quizes[Length(currentMaterial.Quizes) - 1] := newQuiz;
end;

procedure TfrmMaterialEditor.btnSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMaterialEditor.btnAddAssignmntClick(Sender: TObject);
var
  newAssignment: TMaterialAssignment;
begin
  newAssignment := TMaterialAssignment.Create;
  newAssignment.Number := '0';
  newAssignment.Scenario := ScenarioNamesXML[0];
  newAssignment.Text := 'Новое задание';
  SetLength(currentMaterial.Assignments, Length(currentMaterial.Assignments) + 1);
  currentMaterial.Assignments[Length(currentMaterial.Assignments) - 1] := newAssignment;
  frmAssignmentsEditor.EditAssignment(newAssignment);
end;

procedure TfrmMaterialEditor.btnSaveAndExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMaterialEditor.btnCreateQuizClick(Sender: TObject);
begin
end;

procedure TfrmMaterialEditor.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  currentMaterial.Name := lbeName.Text;
  currentMaterial.Number := lbeNumber.Text;
  currentMaterial.ID := cbxClass.Text;

  frmMain.RefreshMaterialsList;
end;

procedure TfrmMaterialEditor.FormCreate(Sender: TObject);
begin

end;

procedure TfrmMaterialEditor.editMaterial(mat: TMaterial);
begin
  currentMaterial := mat;
  lbeName.Text := mat.Name;
  lbeNumber.Text := mat.Number;
  cbxClass.Text := mat.ID;

  RefreshQuizList;

  Show;
end;

procedure TfrmMaterialEditor.RefreshQuizList;
var
  i: integer;
begin
  lbxQuizes.Clear;

  for i := 0 to Length(currentMaterial.Quizes) - 1 do
  begin
    lbxQuizes.Items.Add(currentMaterial.Quizes[i].Number + '. ' +
      currentMaterial.Quizes[i].Name);
  end;
end;

end.
