unit uassignansweredit;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TfrmAssignmentAnswerEditor }

  TfrmAssignmentAnswerEditor = class(TForm)
    Button1: TButton;
    cbxClassList: TComboBox;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    memText: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmAssignmentAnswerEditor: TfrmAssignmentAnswerEditor;

implementation

uses
  ulibrary;

{$R *.lfm}

{ TfrmAssignmentAnswerEditor }

procedure TfrmAssignmentAnswerEditor.FormShow(Sender: TObject);
begin
  cbxClassList.Items := frmLibrary.lbxLib.Items;
end;

procedure TfrmAssignmentAnswerEditor.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAssignmentAnswerEditor.FormCreate(Sender: TObject);
begin

end;

end.
