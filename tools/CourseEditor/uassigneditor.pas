unit uassigneditor;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, umaterials;

type

  { TfrmAssignmentsEditor }

  TfrmAssignmentsEditor = class(TForm)
    Button1: TButton;
    cbxScenario: TComboBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    lbxAssignAnswers: TListBox;
    memAssignText: TMemo;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    txtAssignNumber: TLabeledEdit;
    procedure Button1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    currentAssignment: TMaterialAssignment;
  public
    procedure EditAssignment(assignment: TMaterialAssignment);
    procedure RefreshAswerList;
  end;

var
  frmAssignmentsEditor: TfrmAssignmentsEditor;

implementation

uses
  uassignansweredit, umaterialedit;

{$R *.lfm}

{ TfrmAssignmentsEditor }

procedure TfrmAssignmentsEditor.ToolButton1Click(Sender: TObject);
begin
  frmAssignmentAnswerEditor.Show;
end;

procedure TfrmAssignmentsEditor.EditAssignment(assignment: TMaterialAssignment);
begin
  currentAssignment := assignment;

  txtAssignNumber.Text := currentAssignment.Number;
  memAssignText.Text := currentAssignment.Text;
  cbxScenario.ItemIndex := currentAssignment.GetScenarioIndex(
    currentAssignment.Scenario);
  Show;
end;

procedure TfrmAssignmentsEditor.RefreshAswerList;
var
  i: integer;
begin
  lbxAssignAnswers.Clear;

  for i := 0 to Length(currentAssignment.Answers) - 1 do
  begin
    lbxAssignAnswers.Items.Add(currentAssignment.Answers[i].Text);
  end;
end;

procedure TfrmAssignmentsEditor.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
