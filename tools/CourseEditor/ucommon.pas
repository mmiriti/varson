unit ucommon;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  IXMLProducer = interface
    function GetXML: string;
  end;

implementation

end.

