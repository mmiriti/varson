package misc
{
	import common.ClassFactory;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import layout.Popups;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Help extends Sprite
	{
		private static var _instance:Help;
		
		private static var titles:Array = new Array("СПРАВКА ПО ИСПОЛЬЗОВАНИЮ ИНТЕРФЕЙСА ПРОГРАММЫ", "МЕТОДИЧЕСКИЕ РЕКОМЕНДАЦИИ ДЛЯ УЧИТЕЛЯ К ТАБЛИЦАМ", "ИНФОРМАЦИЯ ОБ АВТОРАХ И РАЗРАБОТЧИКАХ");
		private static var callba:Array = new Array(null, null, null);
		private var defaultTextFormat:TextFormat = new TextFormat("Impact", 36, 0x0, true);
		
		private var _content:Sprite = new Sprite();
		
		public function Help()
		{			
			callba[0] = onInterface;
			callba[1] = onMethod;
			callba[2] = onAbout;
			
			for (var i:int = 0; i < titles.length; i++)
			{
				var title:TextField = new TextField();
				title.defaultTextFormat = defaultTextFormat;
				title.text = (i + 1) + ". " + titles[i];
				title.selectable = false;
				title.multiline = true;
				title.width = 500;
				title.height = 200;
				title.wordWrap = true;
				title.y = i * 200;
				_content.addChild(title);
				
				if (callba[i] != null)
					title.addEventListener(MouseEvent.CLICK, callba[i]);
			}
			
			graphics.beginFill(0xffffff);
			graphics.drawRect(0, 0, _content.width + 50, _content.height + 50)
			graphics.endFill();
			
			_content.x = _content.y = 25;
			
			addChild(_content);
		}
		
		private function onInterface(e:MouseEvent):void
		{
			Popups.instance.hidePopup(function():void
				{
					var helpClass:Class = ClassFactory.getDataClass("HelpInterface");
					Popups.instance.showPopup(new HelpPager(new helpClass()));
				});
		}
		
		private function onMethod(e:MouseEvent):void
		{
			Popups.instance.hidePopup(function():void
				{
					var helpClass:Class = ClassFactory.getDataClass("HelpMethod");
					Popups.instance.showPopup(new HelpPager(new helpClass()));
				});
		}
		
		private function onAbout(e:MouseEvent):void
		{
			Popups.instance.hidePopup(function():void
				{
					var helpClass:Class = ClassFactory.getDataClass("HelpAbout");
					Popups.instance.showPopup(new HelpPager(new helpClass()));
				});
		}
		
		public static function getInstance():Help
		{
			if (_instance == null)
				_instance = new Help();
			return _instance;
		}
	
	}

}