package misc
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author ...
	 */
	public class HelpPager extends Sprite
	{
		public function HelpPager(toPage:Sprite)
		{
			var content:HelpPagerContent = new HelpPagerContent(toPage);
			content.y = 25;
			content.x = 25;
			
			var back:Sprite = new Sprite();
			back.graphics.beginFill(0xffffff);
			back.graphics.drawRect(0, 0, content.width + 50, content.height + 75);
			back.graphics.endFill();
			
			addChild(back);
			addChild(content);
		}
	}

}
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.display.MovieClip;

class HelpPagerContent extends Sprite
{
	private var contentLayer:Sprite = new Sprite();
	private var controlLayer:Sprite = new Sprite();
	
	public function HelpPagerContent(toPage:Sprite)
	{
		
		if (toPage is MovieClip)
		{
			(toPage as MovieClip).stop();
		}
		
		contentLayer.addChild(toPage);
		
		addChild(contentLayer);
		
		if ((toPage is MovieClip) && ((toPage as MovieClip).totalFrames > 1))
		{
			var fwdButton:PageButton = new PageButton();
			fwdButton.x = toPage.width + fwdButton.width * 2;
			fwdButton.y = (contentLayer.height - fwdButton.height) / 2;
			fwdButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
				{
					(toPage as MovieClip).nextFrame();
				});
			
			var bakButton:PageButton = new PageButton(true);
			bakButton.x = 0;
			bakButton.y = (contentLayer.height - fwdButton.height) / 2;
			bakButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
				{
					(toPage as MovieClip).prevFrame();
				});
			
			toPage.x = fwdButton.width;
			
			controlLayer.addChild(bakButton);
			controlLayer.addChild(fwdButton);
		}
		
		addChild(controlLayer);
	}
}

class PageButton extends Sprite
{
	[Embed(source="../../assets/page-button.png")]
	private static var _pageButton:Class;
	
	public function PageButton(flip:Boolean = false):void
	{
		var _bitmap:Bitmap = new _pageButton();
		if (flip)
		{
			_bitmap.rotation = 180;
			_bitmap.y = _bitmap.height;
			_bitmap.x = _bitmap.width;
		}
		
		addChild(_bitmap);
	}
}