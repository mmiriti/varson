package
{
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.text.TextFormat;
	import flash.utils.Dictionary;
	import layout.ApplicationContainer;
	import layout.Bottom;
	import layout.Top;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Main extends Sprite
	{
		private var _applicationContainer:ApplicationContainer;
		private static var _materials:Materials;
		
		public static const W_WIDTH:int = 1280;
		public static const W_HEIGHT:int = 800;
		public static const VIEWPORT:Rectangle = new Rectangle(0, 0, W_WIDTH, W_HEIGHT);
		
		[Embed(source="../dat/data.xml",mimeType="application/octet-stream")]
		private static var xmlData:Class;
		
		public static var generalSubtitle:String;
		public static var generalName:String;
		
		public static var textFormatSmall:TextFormat = new TextFormat("Tahoma", 20, null, true);
		
		public static var assetsClassesDomain:ApplicationDomain = null;
		public static var designDomain:ApplicationDomain = null;
		
		private static var designBitmapDatas:Dictionary = new Dictionary();
		
		private var dynamicXMLData:String;
		
		/**
		 * Точка входа в приложение
		 *
		 */
		public function Main():void
		{
			if(CONFIG::debug == false){
				stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			}
			stage.align = StageAlign.TOP_LEFT;
			// TODO Это может оказаться не плохим вариантом
			// stage.scaleMode = StageScaleMode.EXACT_FIT;
			
			var xmlDataLoader:URLLoader = new URLLoader(new URLRequest("data.xml"));
			xmlDataLoader.addEventListener(IOErrorEvent.IO_ERROR, function():void
				{
					trace("no data found for dynamic load");
					loadStaticData();
				});
			xmlDataLoader.addEventListener(Event.COMPLETE, onDynamicXML);
		}
		
		/**
		 * Получен xml из внешнего файла
		 *
		 * @param	e
		 */
		private function onDynamicXML(e:Event):void
		{
			dynamicXMLData = (e.target as URLLoader).data;
			
			var swfDataLoader:Loader = new Loader();
			swfDataLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onDynamicSWF);
			swfDataLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onDynamicSWFError);
			swfDataLoader.load(new URLRequest("data.swf"));
		}
		
		/**
		 * Ошибка при загрузке внешнего SWF файла
		 *
		 * @param	e
		 */
		private function onDynamicSWFError(e:IOErrorEvent):void
		{
			trace("no dynamic swf data");
			loadStaticData();
		}
		
		/**
		 * Внешний SWF файл загружен
		 *
		 * @param	e
		 */
		private function onDynamicSWF(e:Event):void
		{
			trace("Loading dynamic data");
			assetsClassesDomain = (e.target as LoaderInfo).applicationDomain;
			_parseXml(dynamicXMLData);
			loadDesign();
		}
		
		/**
		 * Загрузить статичные данные
		 *
		 */
		private function loadStaticData():void
		{
			_parseXml(new xmlData());
			loadDesign();
		}
		
		/**
		 * Распарсить XML
		 *
		 * @param	data
		 */
		private function _parseXml(data:*):void
		{
			var xml:XML = new XML(data);
			
			generalName = xml.generalInfo.name;
			generalSubtitle = xml.generalInfo.subtitle;
			
			_materials = new Materials(xml);
		}
		
		/**
		 * Загрузить дизайн
		 *
		 */
		private function loadDesign():void
		{
			trace("Loading design");
			var designSWFLoader:Loader = new Loader();
			designSWFLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onDesignLoaded);
			designSWFLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onDesignLoadError);
			designSWFLoader.load(new URLRequest("design.swf"));
		}
		
		/**
		 * Дизайн загружен из внешнего SWF файла
		 *
		 * @param	e
		 */
		private function onDesignLoaded(e:Event):void
		{
			trace("design loaded");
			designDomain = (e.target as LoaderInfo).applicationDomain;
			startApplication();
		}
		
		/**
		 * Ошибка загрузки дизайна
		 *
		 * @param	e
		 */
		private function onDesignLoadError(e:IOErrorEvent):void
		{
			trace("design load error:", e.text);
			startApplication();
		}
		
		/**
		 * Запустить приложение
		 *
		 */
		private function startApplication():void
		{
			_applicationContainer = new ApplicationContainer();
			addChild(_applicationContainer);
			
			var _mask:Sprite = new Sprite();
			_mask.graphics.beginFill(0x0);
			_mask.graphics.drawRect(0, 0, W_WIDTH, W_HEIGHT);
			_mask.graphics.endFill();
			mask = _mask;
		}
		
		/**
		 * Геттер списка материалов
		 *
		 */
		static public function get materials():Materials
		{
			return _materials;
		}
	
	}

}