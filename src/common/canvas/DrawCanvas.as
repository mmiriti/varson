package common.canvas
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class DrawCanvas extends Sprite
	{
		private var _canvasData:BitmapData;
		private var _eraceData:BitmapData;
		
		private var _isDrawing:Boolean = false;
		private var _currentBrush:DrawBrush;
		private var _matrix:Matrix = new Matrix();
		private var _lastDrawPosition:Point = null;
		private var _isEracing:Boolean = false;
		
		public function DrawCanvas(canvasWidth:int, canvasHeight:int)
		{
			super();
			
			_canvasData = new BitmapData(canvasWidth, canvasHeight, true, 0x00000000);			
			addChild(new Bitmap(_canvasData));			
		}
		
		private function _putPoint(atX:Number, atY:Number):void
		{
			_matrix.identity();
			_matrix.translate(atX, atY);
			if (_isEracing)
			{
				//_canvasData.draw(_eraceData, _matrix);
				_canvasData.copyPixels(_eraceData, _eraceData.rect, new Point(atX - _eraceData.width / 2, atY - _eraceData.height / 2));
			}
			else
			{
				_canvasData.draw(_currentBrush, _matrix);
			}
		}
		
		public function startDraw(penWidth:int, penColor:uint = 0xff0000):void
		{
			_isDrawing = true;
			_currentBrush = new DrawBrush(penWidth, penColor);
		}
		
		public function startErace(penWidth:int):void
		{
			_isDrawing = true;
			_isEracing = true;
			_eraceData = new BitmapData(penWidth / 2, penWidth / 2, true, 0x00000000);
		}
		
		public function doDraw(atX:Number, atY:Number):void
		{
			if (_isDrawing)
			{
				if (_lastDrawPosition == null)
				{
					_lastDrawPosition = new Point(atX, atY);
					_putPoint(atX, atY);
				}
				else
				{
					var p:Point = _lastDrawPosition.clone();
					var tP:Point = new Point(atX, atY);
					var v:Point = new Point(tP.x - p.x, tP.y - p.y);
					if (v.length > 1)
					{
						var ln:int = Math.ceil(v.length);
						v.normalize(1);
						for (var i:int = 0; i < ln; i++)
						{
							var cP:Point = new Point(p.x + v.x * i, p.y + v.y * i);
							_putPoint(cP.x, cP.y);
						}
					}
					else
					{
						_putPoint(atX, atY);
					}
					_lastDrawPosition.copyFrom(tP);
				}
			}
		}
		
		public function endErace():void
		{
			_isDrawing = false;
			_isEracing = false;
		
		}
		
		public function endDraw():void
		{
			_isDrawing = false;
			_lastDrawPosition = null;
		}
	}
}
import flash.display.Sprite;

class DrawBrush extends Sprite
{
	public function DrawBrush(brushSize:Number, brushColor:uint):void
	{
		graphics.beginFill(brushColor);
		graphics.drawCircle(0, 0, brushSize);
		graphics.endFill();
	}
}