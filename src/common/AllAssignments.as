package common
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class AllAssignments extends Sprite
	{
		private var container:Sprite = new Sprite();
		private var _scroll:Boolean = false;
		[Embed(source="../../assets/scrollbar_button.png")]
		private static var _scrollButton:Class;
		private var _downButton:Sprite;
		private var _upButton:Sprite;
		
		public function AllAssignments()
		{
			var matFormat:TextFormat = new TextFormat("Tahoma", 20, 0, false);
			
			var topY:Number = 10;
			
			for (var i:int = 0; i < Main.materials.collection.length; i++)
			{
				var m:Material = Main.materials.collection[i];
				
				if (m.childrenMaterials.length > 0)
				{
					var matTitle:TextField = new TextField();
					matTitle.autoSize = TextFieldAutoSize.LEFT;
					matTitle.defaultTextFormat = matFormat;
					matTitle.text = m.number + ". " + m.name;
					matTitle.selectable = false;
					matTitle.y = topY;
					
					container.addChild(matTitle);
					
					topY += matTitle.height;
					
					for (var j:int = 0; j < m.childrenMaterials.length; j++)
					{
						var testItem:AssignmentItem = new AssignmentItem(m, m.childrenMaterials[j]);
						testItem.y = topY;
						testItem.x = 30;
						
						container.addChild(testItem);
						
						topY += testItem.height;
					}
				}
			}
			
			if (container.height > 600)
			{
				var maskSprite:Sprite = new Sprite();
				maskSprite.graphics.beginFill(0xffffff);
				maskSprite.graphics.drawRect(0, 0, container.width + 80, 600);
				maskSprite.graphics.endFill();
				mask = maskSprite;
				addChild(maskSprite);
				
				_upButton = new Sprite();
				_upButton.addChild(new _scrollButton());
				_upButton.x = container.width + 60 - _upButton.width;
				
				_downButton = new Sprite();
				var bmp:Bitmap = new _scrollButton();
				bmp.scaleY = -1;
				_downButton.addChild(bmp);
				_downButton.x = container.width + 60 - _downButton.width;
				_downButton.y = 600;
				
				_downButton.addEventListener(MouseEvent.MOUSE_DOWN, onScrollDown);
				_upButton.addEventListener(MouseEvent.MOUSE_DOWN, onScrollUp);
				
				_scroll = true;
			}
			
			var background:Sprite = new Sprite();
			background.graphics.beginFill(0xffffff);
			background.graphics.drawRect(0, 0, container.width + 80, container.height);
			background.graphics.endFill();
			
			addChild(background);
			container.x = 20;
			addChild(container);
			
			if ((_upButton != null) && (_downButton != null))
			{
				addChild(_upButton);
				addChild(_downButton);
			}
			
			addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		}
		
		override public function get height():Number
		{
			if (super.height < 600)
				return super.height;
			else
				return 600;
		}
		
		override public function set height(value:Number):void
		{
			super.height = value;
		}
		
		private function onScrollUp(e:MouseEvent):void
		{
			scrollAll(10);
		}
		
		private function onScrollDown(e:MouseEvent):void
		{
			scrollAll(-10);
		}
		
		private function scrollAll(delta:int):void
		{
			container.y += delta;
			
			if (container.y > 0)
				container.y = 0;
			
			if ((container.y + container.height) < 580)
			{
				container.y = -container.height + 580;
			}
		}
		
		private function onMouseWheel(e:MouseEvent):void
		{
			if (_scroll)
			{
				scrollAll(e.delta * 5);
			}
		}
	}
}

import common.Material;
import common.QuizMaterial;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import layout.ApplicationContainer;
import layout.MainViewContainer;
import layout.Popups;
import layout.toolbars.bottom_buttons.AssignmentsButton;
import layout.toolbars.ToolBarButtonState;
import layout.ViewContainer;
import layout.views.MaterialView;

class AssignmentItem extends Sprite
{
	private var _material:Material;
	private static var testFormat:TextFormat = new TextFormat("Tahoma", 24, 0, true);
	private var _parentMaterial:Material;
	
	public function AssignmentItem(parentMaterial:Material, material:Material)
	{
		_parentMaterial = parentMaterial;
		_material = material;
		
		var testTitle:TextField = new TextField();
		testTitle.autoSize = TextFieldAutoSize.LEFT;
		testTitle.defaultTextFormat = testFormat;
		testTitle.text = material.number + ". " + material.name;
		testTitle.selectable = false;
		addChild(testTitle);
		
		addEventListener(MouseEvent.CLICK, onClick);
	}
	
	private function onClick(e:MouseEvent):void
	{
		var materialToView:MaterialView = new MaterialView(600, _parentMaterial, false);
		materialToView.bestView();
		
		AssignmentsButton.Active = true;
		AssignmentsButton.Testing = materialToView;
		AssignmentsButton.Instance.state = ToolBarButtonState.ACTIVE;
		
		Popups.instance.hidePopup();
		
		ApplicationContainer.I.resetMainViewContainer(new MainViewContainer());
		
		MainViewContainer.I.clearAll();
		MainViewContainer.I.putLeft(new ViewContainer(600).loadView(AssignmentsButton.Testing));
		MainViewContainer.I.getLeft().currentView.bestView();
		MainViewContainer.I.putRight(new ViewContainer(600, false).loadView(new MaterialView(600, (_material as QuizMaterial).reset(), false)));
	}
}