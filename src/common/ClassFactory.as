package common
{
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ClassFactory
	{
		private static var _singletones:Dictionary = new Dictionary();
		
		public static function getDataClass(name:String):Class
		{
			if (Main.assetsClassesDomain == null)
			{
				return getDefinitionByName(name) as Class;
			}
			else
			{
				return Main.assetsClassesDomain.getDefinition(name) as Class;
			}
		}
		
		public static function getDesignClass(name:String):Class
		{
			if (Main.designDomain == null)
			{
				return getDefinitionByName(name) as Class;
			}
			else
			{
				return Main.designDomain.getDefinition(name) as Class;
			}
		}
		
		public static function getSingletone(byClass:Class):*
		{
			if (_singletones[byClass] == undefined)
			{
				_singletones[byClass] = new byClass();
			}
			return _singletones[byClass];
		}
	}

}