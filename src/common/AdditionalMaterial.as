package common
{
	import common.canvas.DrawCanvas;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import layout.toolbars.additional_button.AdditionalEracer;
	import layout.toolbars.additional_button.AdditionalPencilButton;
	import layout.toolbars.ToolbarAdditionInfo;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class AdditionalMaterial extends Sprite
	{
		private var toolbar:ToolbarAdditionInfo = new ToolbarAdditionInfo();
		private var _canvas:DrawCanvas;
		private var _lastDrawPos:Point = null;
		static private const MAX_WIDTH:Number = 1000;
		
		public function AdditionalMaterial(content:DisplayObject, borderFields:int = 10)
		{
			graphics.beginFill(0xffffff);
			
			if (content.width > MAX_WIDTH)
			{
				var scaleTo:Number = MAX_WIDTH / content.width;
				content.scaleX = content.scaleY = scaleTo;
			}
			
			graphics.drawRect(0, 0, content.width + borderFields * 2 + toolbar.width, Math.max(content.height, toolbar.height) + borderFields * 2);
			graphics.endFill();
			
			content.x = borderFields + toolbar.width;
			content.y = borderFields;
			addChild(content);
			
			_canvas = new DrawCanvas(content.width, Math.max(content.height, toolbar.height));
			_canvas.x = content.x;
			_canvas.y = content.y;
			addChild(_canvas);
			
			addChild(toolbar);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			var pos:Point = _canvas.globalToLocal(new Point(e.stageX, e.stageY));
			_lastDrawPos = pos;
			
			if (AdditionalPencilButton.enabled)
			{
				_canvas.startDraw(2, 0x0);
			}
			
			if (AdditionalEracer.enabled)
			{
				_canvas.startErace(60);
			}
		}
		
		private function onMouseUp(e:MouseEvent):void
		{
			_lastDrawPos = null;
			_canvas.endDraw();
			_canvas.endErace();
		}
		
		private function onMouseMove(e:MouseEvent):void
		{
			if (_lastDrawPos != null)
			{
				_lastDrawPos.copyFrom(_canvas.globalToLocal(new Point(e.stageX, e.stageY)));
				_canvas.doDraw(_lastDrawPos.x, _lastDrawPos.y);
			}
		}
	
	}

}