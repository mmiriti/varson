package common
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.utils.getQualifiedClassName;
	import layout.Popups;
	
	/**
	 * Материал
	 *
	 * @author Michael Miriti
	 */
	public class Material
	{
		protected var _number:String;
		protected var _showBorder:Boolean = true;
		protected var _name:String;
		protected var _displayObject:DisplayObject;
		protected var _childrenMaterials:Vector.<Material> = new Vector.<Material>();
		protected var _additionInfoButtons:Vector.<MovieClip> = new Vector.<MovieClip>();
		protected var _showAdditionalInfo:Boolean = false;
		protected var _haveAdditionalInfo:Boolean = false;
		
		public function Material(nName:String, className:String = "", number:String = "")
		{
			_number = number;
			_name = nName;
			if (className != "")
			{
				var ClassReference:Class = ClassFactory.getDataClass(className);
				
				_displayObject = (new ClassReference()) as DisplayObject;
				
				if (_displayObject is DisplayObjectContainer)
				{
					for (var i:int = 0; i < (_displayObject as DisplayObjectContainer).numChildren; i++)
					{
						var child:DisplayObject = (_displayObject as DisplayObjectContainer).getChildAt(i);
						var className:String = getQualifiedClassName(child);						
						if (className.search("info_button") == 0)
						{
							_haveAdditionalInfo = true;
							_additionInfoButtons.push(child);
							child.alpha = 0;
							(child as MovieClip).useHandCursor = true;
							child.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
								{
									var myClassName:String = getQualifiedClassName(e.target);
									var infoName:String = myClassName.replace("info_button", "");
									var infoClass:Class = ClassFactory.getDataClass("info" + infoName);
									Popups.instance.showPopup(new AdditionalMaterial(ClassFactory.getSingletone(infoClass)));
								});
						}
					}
				}
			}
		}
		
		public function addChildMaterial(child:Material):void
		{
			_childrenMaterials.push(child);
		}
		
		public function get displayObject():DisplayObject
		{
			return _displayObject;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function get childrenMaterials():Vector.<Material>
		{
			return _childrenMaterials;
		}
		
		public function get showBorder():Boolean
		{
			return _showBorder;
		}
		
		public function set showBorder(value:Boolean):void
		{
			_showBorder = value;
		}
		
		public function get number():String
		{
			return _number;
		}
		
		public function get showAdditionalInfo():Boolean
		{
			return _showAdditionalInfo;
		}
		
		public function set showAdditionalInfo(value:Boolean):void
		{
			for (var i:int = 0; i < _additionInfoButtons.length; i++)
			{
				if (value)
				{
					_additionInfoButtons[i].alpha = 1;
				}
				else
				{
					_additionInfoButtons[i].alpha = 0;
				}
			}
			
			_showAdditionalInfo = value;
		}
		
		public function get haveAdditionalInfo():Boolean
		{
			return _haveAdditionalInfo;
		}
	}

}