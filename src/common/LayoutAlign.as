package common
{
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class LayoutAlign
	{
		public static const NONE:String = "none";
		public static const TOP:String = "top";
		public static const BOTTOM:String = "bottom";
		public static const LEFT:String = "left";
		public static const RIGHT:String = "right";
		public static const CLIENT:String = "client";
	}

}