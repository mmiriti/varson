package common.quizparts
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class QuizOptionCheck extends Sprite
	{
		[Embed(source="../../../assets/checkbox/checkbox-unchecked_disabled.png")]
		private static var _checkbox_unchecked:Class;
		
		[Embed(source="../../../assets/checkbox/checkbox-checked.png")]
		private static var _checkbox_checked:Class;
		
		private var checked:Bitmap = new _checkbox_checked();
		private var unchecked:Bitmap = new _checkbox_unchecked();
		
		private var _on:Boolean = false;
		
		public function QuizOptionCheck()
		{
			super();
			
			addChild(unchecked);
		
		}
		
		public function get on():Boolean
		{
			return _on;
		}
		
		public function set on(value:Boolean):void
		{
			if (!value)
			{
				if (_on)
				{
					removeChild(checked);
					addChild(unchecked);
				}
			}
			else
			{
				if (!_on)
				{
					removeChild(unchecked);
					addChild(checked);
				}
			}
			_on = value;
		}
	
	}

}