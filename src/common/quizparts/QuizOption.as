package common.quizparts
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class QuizOption extends Sprite
	{
		private var _title:TextField;
		private var _correct:Boolean = false;
		static private var _textFormat:TextFormat = new TextFormat(null, 18, 0x0, false);
		private var _checkbox:QuizOptionCheck = new QuizOptionCheck();
		
		public function QuizOption(xmlNode:XML)
		{
			super();
			
			addChild(_checkbox);
			
			_checkbox.addEventListener(MouseEvent.CLICK, onCheckClick);
			
			_title = new TextField();
			_title.text = xmlNode;
			_title.selectable = false;
			_title.autoSize = TextFieldAutoSize.LEFT;
			_title.setTextFormat(_textFormat);
			_title.x = _checkbox.width + 5;
			_title.y = (_checkbox.height - _title.height) / 2;
			addChild(_title);
			
			_correct = (xmlNode.@correct == "true");
		}
		
		public function unSelect():void
		{
			_checkbox.on = false;
		}
		
		public function reset():void
		{
			_title.setTextFormat(_textFormat);
			_checkbox.on = false;
		}
		
		public function check():void
		{
			var tf:TextFormat = _title.getTextFormat();
			
			if (_correct)
			{
				tf.color = 0x00aa00;
				tf.bold = true;
				_title.setTextFormat(tf);
			}
			else
			{
				if (_checkbox.on)
				{
					tf.color = 0xff0000;
					tf.bold = true;
					_title.setTextFormat(tf);
				}
			}
		}
		
		private function onCheckClick(e:MouseEvent):void
		{
			_checkbox.on = !_checkbox.on;
			(parent as QuizQuest).selectedOption(this);
		}
		
		public function get checkbox():QuizOptionCheck
		{
			return _checkbox;
		}
	
	}

}