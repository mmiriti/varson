package common.quizparts
{
	import common.ClassFactory;
	import common.Material;
	import common.QuizMaterial;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class QuizQuest extends Sprite
	{
		private static var _textFormat:TextFormat = new TextFormat(null, 24, 0x0, true);
		private var _questTitle:TextField;
		private var _options:Vector.<QuizOption> = new Vector.<QuizOption>();
		private var _parentMaterial:QuizMaterial;
		private var _isMultiple:Boolean;
		
		public function QuizQuest(parentMaterial:QuizMaterial, xmlNode:XML)
		{
			super();
			_isMultiple = (xmlNode.@multiple == "true");
			
			_parentMaterial = parentMaterial;
			_questTitle = new TextField();
			_questTitle.text = xmlNode.@title;
			_questTitle.setTextFormat(_textFormat);
			_questTitle.multiline = true;
			_questTitle.selectable = false;
			_questTitle.autoSize = TextFieldAutoSize.LEFT;
			_questTitle.wordWrap = true;
			_questTitle.width = 600;
			addChild(_questTitle);
			
			var nextY:Number;
			
			var imageClass:String = xmlNode.@image;
			if (imageClass != "")
			{
				var ClassReference:Class = ClassFactory.getDataClass(imageClass);
				var questImage:Object = new ClassReference();
				var image:DisplayObject;
				
				if (questImage is BitmapData)
				{
					var imageData:BitmapData = (new ClassReference()) as BitmapData;
					image = new Bitmap(imageData);
				}
				else if (questImage is MovieClip)
				{
					image = questImage as MovieClip;
				}else {
					throw new Error('invalid image type');
				}
				image.y = _questTitle.height + 5;
				nextY = image.y + image.height + 5;
				addChild(image);
			}
			else
			{
				nextY = _questTitle.height + 5;
			}
			
			for (var i:int = 0; i < xmlNode.option.length(); i++)
			{
				var newOption:QuizOption = new QuizOption(xmlNode.option[i]);
				newOption.y = nextY;
				nextY += newOption.height;
				addChild(newOption);
				
				_options.push(newOption);
			}
		}
		
		public function selectedOption(quizOption:QuizOption):void
		{
			if (!_isMultiple)
			{
				for (var i:int = 0; i < _options.length; i++)
				{
					if (_options[i] != quizOption)
					{
						_options[i].unSelect();
					}
				}
			}
		
		}
		
		public function reset():void
		{
			for (var i:int = 0; i < _options.length; i++)
			{
				_options[i].reset();
			}
		}
		
		public function check():void
		{
			var show:Boolean = false;
			for (var j:int = 0; j < _options.length; j++)
			{
				if (_options[j].checkbox.on)
				{
					show = true;
					break;
				}
			}
			if (show)
			{
				for (var i:int = 0; i < _options.length; i++)
				{
					_options[i].check();
				}
			}
		}
	
	}

}