package common
{
	import common.quizparts.QuizQuest;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class QuizMaterial extends Material
	{
		private var _title:TextField;
		private var _quzes:Vector.<QuizQuest> = new Vector.<QuizQuest>();
		private var _back:Sprite;
		
		public function QuizMaterial(parentMaterial:Material, xmlNode:XML)
		{
			super(xmlNode.@name, "", xmlNode.@number);
			
			var questDoc:Sprite = new Sprite();
			_showBorder = false;
			
			_back = new Sprite();
						
			questDoc.addChild(_back);
			
			_title = new TextField();
			_title.text = xmlNode.@name;
			_title.setTextFormat(new TextFormat(null, 36, 0x0, true));
			_title.autoSize = TextFieldAutoSize.LEFT;
			_title.selectable = false;
			questDoc.addChild(_title);
			
			var _lastY:Number = _title.height + 20;
			
			for (var i:int = 0; i < xmlNode.quest.length(); i++)
			{
				var newQuest:QuizQuest = new QuizQuest(this, xmlNode.quest[i]);
				newQuest.y = _lastY;
				_lastY += newQuest.height + 15;
				questDoc.addChild(newQuest);
				_quzes.push(newQuest);
			}
			
			var _checkButton:CheckButton = new CheckButton();
			_checkButton.x = (questDoc.width - _checkButton.width) / 2;
			_checkButton.y = _lastY + 20;
			questDoc.addChild(_checkButton);
			
			_checkButton.addEventListener(MouseEvent.CLICK, onCheckClick);
			
			_back.graphics.beginFill(0xffffff);
			_back.graphics.lineStyle(1);
			_back.graphics.drawRect(-10, -10, questDoc.width + 20, questDoc.height + 20);
			_back.graphics.endFill();
			
			_displayObject = questDoc;
		}
		
		public function reset():QuizMaterial
		{
			for (var i:int = 0; i < _quzes.length; i++)
			{
				_quzes[i].reset();
			}
			
			return this;
		}
		
		private function onCheckClick(e:MouseEvent):void
		{
			checkAllQuiz();
		}
		
		private function checkAllQuiz():void
		{
			for (var i:int = 0; i < _quzes.length; i++)
			{
				_quzes[i].check();
			}
		}		
	}

}

import flash.display.Sprite;
import flash.filters.DropShadowFilter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

class CheckButton extends Sprite
{
	private var _caption:TextField;
	
	public function CheckButton()
	{
		graphics.beginFill(0x00aa00);
		graphics.lineStyle(2, 0x008800);
		graphics.drawRoundRect(0, 0, 200, 50, 10, 10);
		graphics.endFill();
		filters = [new DropShadowFilter()];
		
		_caption = new TextField();
		_caption.text = "Проверить";
		_caption.setTextFormat(new TextFormat(null, 30, 0xffffff, true));
		_caption.selectable = false;
		_caption.width = 200;
		_caption.height = 50;
		_caption.autoSize = TextFieldAutoSize.CENTER;
		addChild(_caption);
		
		useHandCursor = true;
	}
}