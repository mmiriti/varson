package
{
	import common.Material;
	import common.QuizMaterial;
	
	/**
	 * Коллекция материалов.
	 *
	 * Разбор из XML
	 *
	 * @author Michael Miriti
	 */
	public class Materials
	{
		private var _collection:Vector.<Material> = new Vector.<Material>();
		
		public function Materials(xml:XML)
		{
			for (var i:int = 0; i < xml.materialsList.material.length(); i++)
			{
				var name:String = xml.materialsList.material[i].@name;
				var number:String = xml.materialsList.material[i].@number;
				var className:String = xml.materialsList.material[i].@id;
				var newMaterial:Material = new Material(name, className, number);
				
				if (xml.materialsList.material[i].children().length() != 0)
				{
					for (var j:int = 0; j < xml.materialsList.material[i].quiz.length(); j++)
					{
						newMaterial.addChildMaterial(new QuizMaterial(newMaterial, xml.materialsList.material[i].quiz[j]));
					}
				}
				
				_collection.push(newMaterial);
			}
		}
		
		public function get collection():Vector.<Material>
		{
			return _collection;
		}
	
	}

}