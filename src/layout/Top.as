package layout
{
	import common.ClassFactory;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import layout.toolbars.FullscreenButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Top extends LayoutElement
	{
		private var _title:TextField;
		private var _number:TextField;
		private var _subtitle:TextField;
		private var _fullscreenButton:FullscreenButton;
		
		static public var I:Top;
		
		public function Top()
		{
			super();
			
			I = this;
			
			graphics.beginFill(0xffcccccc);
			graphics.drawRect(0, 0, Main.W_WIDTH, 70);
			graphics.endFill();
			
			var bitmapDataClass:Class = ClassFactory.getDesignClass("DesignHeader");
			var backImage:Bitmap = new Bitmap(new bitmapDataClass());
			addChild(backImage);
			
			_title = new TextField();
			_title.selectable = false;
			_title.defaultTextFormat = new TextFormat("Verdana", 24, null, true, null, null, null, null, TextFormatAlign.CENTER);
			_title.text = Main.generalName;
			_title.autoSize = TextFieldAutoSize.CENTER;
			_title.x = (1280 - _title.width) / 2;
			addChild(_title);
			
			_subtitle = new TextField();
			_subtitle.selectable = false;
			_subtitle.defaultTextFormat = new TextFormat("Verdana", 24, null, false, null, null, null, null, TextFormatAlign.CENTER);
			_subtitle.autoSize = TextFieldAutoSize.CENTER;
			_subtitle.text = Main.generalSubtitle;
			_subtitle.x = (1280 - _subtitle.width) / 2;
			_subtitle.y = 35;
			addChild(_subtitle);
			
			_fullscreenButton = new FullscreenButton();
			
			_fullscreenButton.x = width - _fullscreenButton.width - 10;
			_fullscreenButton.y = 10;
			addChild(_fullscreenButton);
		
		}
	
	}

}