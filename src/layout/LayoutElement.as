package layout
{
	import common.AppSprite;
	import common.LayoutAlign;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import layout.toolbars.main_buttons.PoinerButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class LayoutElement extends AppSprite
	{
		protected var _align:String = LayoutAlign.NONE;
		protected var _isScroll:Boolean = false;
		protected var _scrollContainer:Sprite;
		
		private var _scrollArea:Sprite;
		private var _mask:Sprite;		
		private var _scrollDrag:Boolean = false;
		static private const MAX_OUTFIT:Number = 50;
		
		public function LayoutElement()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function _initScroll(w:int, h:int):void
		{
			_mask = new Sprite();
			_mask.graphics.beginFill(0xffffff);
			_mask.graphics.drawRect(0, 0, w, h);
			_mask.graphics.endFill();
			
			_scrollArea = new Sprite();
			_scrollArea.addChild(_mask);
			_scrollArea.mask = _mask;
			addChild(_scrollArea);
			
			_scrollContainer = new Sprite();
			_scrollArea.addChild(_scrollContainer);
			
			_scrollContainer.addEventListener(MouseEvent.MOUSE_DOWN, onScrollContainerMouseDown);
			_scrollContainer.addEventListener(MouseEvent.MOUSE_WHEEL, onScrollContainerMouseWheel);
			
			_isScroll = true;
		}
		
		private function onScrollContainerMouseWheel(e:MouseEvent):void
		{
			_scrollContainer.y += e.delta * 5;
		}
		
		private function onScrollContainerMouseMove(e:MouseEvent):void
		{
			if (_scrollDrag)
			{
				if (_scrollContainer.x - _scrollContainer.width / 2 > _mask.width - MAX_OUTFIT)
				{
					_scrollContainer.x = _mask.width - MAX_OUTFIT + _scrollContainer.width / 2;
				}
				
				if (_scrollContainer.x + _scrollContainer.width / 2 < MAX_OUTFIT)
				{
					_scrollContainer.x = MAX_OUTFIT - _scrollContainer.width / 2;
				}
				
				if (_scrollContainer.y < -_scrollContainer.height + MAX_OUTFIT)
				{
					_scrollContainer.y = -_scrollContainer.height + MAX_OUTFIT;
				}
				
				if (_scrollContainer.y > _mask.height - MAX_OUTFIT)
				{
					_scrollContainer.y = _mask.height - MAX_OUTFIT;
				}
				
				e.updateAfterEvent();
			}
		}
		
		private function onScrollContainerMouseDown(e:MouseEvent):void
		{
			if ((_scrollDrag) && (PoinerButton.enabled))
				_scrollContainer.startDrag();
		}
		
		private function onScrollContainerMouseUp(e:MouseEvent):void
		{
			if ((_scrollDrag) && (PoinerButton.enabled))
				_scrollContainer.stopDrag();
		}
		
		public function scrollEnableDrag():void
		{
			_scrollDrag = true;
		}
		
		public function scrollDisableDrag():void
		{
			_scrollDrag = false;
		}
		
		public function addToScroll(child:DisplayObject):DisplayObject
		{
			if (child != null)
			{
				if (_isScroll)
				{
					_scrollContainer.addChild(child);
					_scrollArea.mask = _mask;
				}
			}
			return _scrollContainer;
		}
		
		protected function _drawPlaceholder(col:uint, w:int, h:int):void
		{
			graphics.beginFill(col);
			graphics.drawRect(0, 0, w, h);
			graphics.endFill();
		}
		
		protected function onAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onScrollContainerMouseMove);
			stage.addEventListener(MouseEvent.MOUSE_UP, onScrollContainerMouseUp);
			
			switch (_align)
			{
				case LayoutAlign.BOTTOM: 
					y = parent.height - height;
					break;
				case LayoutAlign.LEFT: 
					x = 0;
					break;
				case LayoutAlign.RIGHT: 
					x = parent.width - width;
					break;
				case LayoutAlign.TOP: 
					y = 0;
					break;
				case LayoutAlign.CLIENT: 
					x = 0;
					y = 0;
					width = parent.width;
					height = parent.height;
					break;
			}
		}
		
		public function get scrollContainer():Sprite
		{
			return _scrollContainer;
		}
	}
}