package layout
{
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	import layout.views.AllMaterials;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ApplicationContainer extends LayoutElement
	{
		public static var I:ApplicationContainer;
		
		private var _elementsLayer:Sprite = new Sprite();
		
		private var _top:Top;
		private var _left:Left;
		private var _bottom:Bottom;
		private var _main:MainViewContainer;
		
		public var topmostLayer:Sprite = new Sprite();
		
		public function ApplicationContainer()
		{
			super();
			I = this;
			_left = new Left();
			
			_drawPlaceholder(0xffffff, Main.W_WIDTH, Main.W_HEIGHT);
			
			_elementsLayer.addChild(_top = new Top());
			
			_elementsLayer.addChild(_bottom = new Bottom());
			resetMainViewContainer(new MainViewContainer(Main.W_WIDTH));
			_elementsLayer.addChild(_left);
			
			addChild(_elementsLayer);
			addChild(topmostLayer);
			
			_bottom.y = 800 - _bottom.height;
			
			_left.y = _top.y + _top.height;
			
			_main.putFull(new ViewContainer(Main.W_WIDTH, "reset").loadView(new AllMaterials(Main.W_WIDTH)));
		}
		
		public function resetMainViewContainer(newContainer:MainViewContainer):void
		{
			if (_main != null)
			{
				_elementsLayer.removeChild(_main);
			}
			_main = newContainer;
			_main.x = Main.W_WIDTH - _main.width;
			_main.y = _top.y + _top.height;
			_elementsLayer.addChild(_main);
			
			_left.x = _main.x - _left.width;
		}
		
		public function get top():Top
		{
			return _top;
		}
		
		public function get left():Left
		{
			return _left;
		}
		
		public function get bottom():Bottom
		{
			return _bottom;
		}
		
		public function get main():MainViewContainer
		{
			return _main;
		}
	
	}

}