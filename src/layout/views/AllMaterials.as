package layout.views
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.Dictionary;
	import layout.LayoutElement;
	import layout.toolbars.ToolBarMain;
	import layout.ViewContainer;
	
	/**
	 * Просмотр списка (галлереи) всех имеющихся материалов
	 *
	 * @author Michael Miriti
	 */
	public class AllMaterials extends View
	{
		public static const THUMB_WIDTH:int = 270;
		public static const THUMB_HEIGHT:int = 180;
		public static const THUMB_COLS:int = 4;
		static public const CONTENT_NAME:String = "- Список всех материалов -";
		
		private static var _padding:Number = 10;
		
		private var _tumbsContainer:Sprite;
		
		public function AllMaterials(containerWidth:int = 1280)
		{
			super(containerWidth);
			_initScroll(containerWidth, 650);
			
			var col:int = 0;
			var row:int = 0;
			
			for (var i:int = 0; i < Main.materials.collection.length; i++)
			{
				var tmb:MaterialThumb = new MaterialThumb(this, Main.materials.collection[i]);
				tmb.x = _padding + (col * (THUMB_WIDTH + _padding));
				tmb.y = _padding + (row * (THUMB_HEIGHT + 25 + _padding));
				addToScroll(tmb);
				
				col++;
				if (col == THUMB_COLS)
				{
					row++;
					col = 0;
				}
			}
		}
		
		override public function reProduce(containerWidth:int):View
		{
			return new AllMaterials(containerWidth);
		}
		
		override public function onView(onContainer:ViewContainer):void
		{
			super.onView(onContainer);
			onContainer.contentDropDown.setCaption(CONTENT_NAME);
		}
	
	}

}
import common.Material;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.filters.DropShadowFilter;
import flash.filters.GlowFilter;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import layout.ApplicationContainer;
import layout.ContentDropDown;
import layout.MainViewContainer;
import layout.ViewContainer;
import layout.views.AllMaterials;
import layout.views.MaterialView;

class MaterialThumb extends Sprite
{
	private var _material:Material;
	private static var _dropShadowFilter:GlowFilter = new GlowFilter(0xcccccc, 1, 5, 5, 1, 1);
	private var _view:AllMaterials;
	static private var _textFormat:TextFormat = new TextFormat("Arial", 10, 0x0, true, false, false, null, null, TextFormatAlign.CENTER);
	
	public function MaterialThumb(view:AllMaterials, material:Material):void
	{
		_view = view;
		if (material.displayObject != null)
		{
			var bmpData:BitmapData = new BitmapData(AllMaterials.THUMB_WIDTH, AllMaterials.THUMB_HEIGHT, true);
			var matrix:Matrix = new Matrix();
			var totalScale:Number;
			
			material.displayObject.scaleX = material.displayObject.scaleY = 1;
			
			if (material.displayObject.width > material.displayObject.height)
				totalScale = AllMaterials.THUMB_WIDTH / material.displayObject.width;
			else
				totalScale = AllMaterials.THUMB_HEIGHT / material.displayObject.height;
			
			matrix.scale(totalScale, totalScale);
			matrix.translate((AllMaterials.THUMB_WIDTH - material.displayObject.width * totalScale) / 2, (AllMaterials.THUMB_HEIGHT - material.displayObject.height * totalScale) / 2);
			bmpData.draw(material.displayObject, matrix);
			var tmbBitmap:Bitmap = new Bitmap(bmpData);
			tmbBitmap.filters = [_dropShadowFilter];
			addChild(tmbBitmap);
			
			var tmbLabel:TextField = new TextField();
			tmbLabel.defaultTextFormat = _textFormat;
			tmbLabel.y = tmbBitmap.height + 5;
			tmbLabel.text = material.number + ". " + material.name;
			tmbLabel.width = tmbBitmap.width;
			tmbLabel.selectable = false;
			addChild(tmbLabel);
			
			_material = material;
			useHandCursor = true;
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
	}
	
	private function onMouseDown(e:MouseEvent):void
	{
		ApplicationContainer.I.resetMainViewContainer(new MainViewContainer());
		var newView:ViewContainer = new ViewContainer();
		newView.loadView(new MaterialView(1205, _material));
		MainViewContainer.I.putFull(newView);
	}
}