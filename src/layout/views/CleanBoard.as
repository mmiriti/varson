package layout.views
{
	import common.canvas.DrawCanvas;
	import flash.events.MouseEvent;
	import layout.ViewContainer;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class CleanBoard extends DrawableView
	{
		public function CleanBoard(containerWidth:int = 1205)
		{
			super(containerWidth);
			_initScroll(containerWidth, 650);
			scrollEnableDrag();
			initDraw(1500, 1000);
			_canvas.y = (650 - _canvas.height) / 2;
			bestView();
		}
		
		override public function bestView():void
		{
			scrollContainer.x = _containerWidth / 2;
			super.bestView();
		}
		
		override public function onView(onContainer:ViewContainer):void
		{
			onContainer.contentDropDown.setCaption("Чистый лист");
		}
	
	}

}