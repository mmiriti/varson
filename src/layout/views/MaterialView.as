package layout.views
{
	import com.greensock.TweenLite;
	import common.canvas.DrawCanvas;
	import common.Material;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import layout.ContentDropDown;
	import layout.LayoutElement;
	import layout.toolbars.main_buttons.ColorPickerButton;
	import layout.toolbars.main_buttons.LineButton;
	import layout.toolbars.main_buttons.TextButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.toolbars.ToolBarMain;
	import layout.ViewContainer;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class MaterialView extends DrawableView
	{
		private var _material:Material;
		private var _showAdditionalInfo:Boolean = false;
		private static var _filters:Array = [];
		
		public function MaterialView(containerWidth:int, material:Material, isdrawable:Boolean = true)
		{
			super(containerWidth);
			_material = material;
			_initScroll(containerWidth, 650);
			addToScroll(_material.displayObject);
			
			_material.displayObject.scaleX = _material.displayObject.scaleY = 1;
			
			if (isdrawable)
			{
				initDraw(containerWidth, 650 * 1.5);
				_canvas.y = (650 - _canvas.height) / 2;
			}
			
			scrollEnableDrag();
			
			if (_material.showBorder)
			{
				scrollContainer.filters = _filters;
			}
			
			bestView();			
		}
		
		override public function reProduce(containerWidth:int):View
		{
			return new MaterialView(containerWidth, _material);
		}
		
		override public function bestView():void
		{
			if (_material.displayObject.width > _material.displayObject.height)
			{
				var targetWidth:Number = _containerWidth * 0.85;
				_material.displayObject.scaleX = _material.displayObject.scaleY = (targetWidth / _material.displayObject.width);
			}
			else
			{
				var targetHeight:Number = 620;
				_material.displayObject.scaleX = _material.displayObject.scaleY = (targetHeight / _material.displayObject.height);
			}
			
			_material.displayObject.x = -_material.displayObject.width / 2;
			_material.displayObject.y = 20;
			scrollContainer.x = _containerWidth / 2;
		}
		
		override public function onView(container:ViewContainer):void
		{
			container.contentDropDown.setCaption(_material.name);
		}
		
		public function eraceAll():void
		{
		}
		
		public function get material():Material
		{
			return _material;
		}
		
		public function get showAdditionalInfo():Boolean
		{
			return _showAdditionalInfo;
		}
		
		public function set showAdditionalInfo(value:Boolean):void
		{
			if (value)
			{
				drawable = false;
				_material.showAdditionalInfo = true;
			}
			else
			{
				drawable = true;
				_material.showAdditionalInfo = false;
			}
			
			_showAdditionalInfo = value;
		}
	
	}

}