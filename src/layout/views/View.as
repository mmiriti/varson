package layout.views
{
	import layout.LayoutElement;
	import layout.ViewContainer;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class View extends LayoutElement
	{
		protected var _container:ViewContainer;
		protected var _containerWidth:int;
		
		public function View(containerWidth:int)
		{
			super();
			_containerWidth = containerWidth;
		}
		
		public function reProduce(containerWidth:int):View
		{
			return null;
		}
		
		public function zoomIn():void
		{
		
		}
		
		public function zoomOut():void
		{
		
		}
		
		public function bestView():void
		{
		
		}
		
		public function onView(onContainer:ViewContainer):void
		{
			_container = onContainer;
		}
		
		public function get container():ViewContainer
		{
			return _container;
		}
	
	}

}