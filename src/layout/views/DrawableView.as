package layout.views
{
	import com.greensock.TweenLite;
	import common.canvas.DrawCanvas;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import layout.toolbars.main_buttons.ColorPickerButton;
	import layout.toolbars.main_buttons.LineButton;
	import layout.toolbars.main_buttons.TextButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.toolbars.ToolBarMain;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class DrawableView extends View
	{
		private var _textFormat:TextFormat = new TextFormat("Times New Roman", 36);
		
		private var _lastDrawPos:Point;
		private var _enableDraw:Boolean;
		private var _enableErace:Boolean;
		private var _enableText:Boolean;
		private var _editText:Boolean = false;
		private var _currentEditText:TextField;
		
		protected var _drawable:Boolean = false;
		protected var _canvas:DrawCanvas;
		
		public function DrawableView(containerWidth:int)
		{
			super(containerWidth);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			ColorPickerButton.addChangeColorHook(changeColorForLastEditedText);
		}
		
		private function changeColorForLastEditedText(color:uint):void
		{
			if (_currentEditText != null)
			{
				_currentEditText.textColor = color;
			}
		}
		
		private function onTextFieldFocusOut(e:FocusEvent):void
		{
			_editText = false;
		}
		
		private function onTextFieldMouseDown(e:MouseEvent):void
		{
			_currentEditText = (e.target as TextField);
			_editText = true;
		}
		
		private function onTextFocusIn(e:FocusEvent):void
		{
			ToolBarMain.I.setState(TextButton, ToolBarButtonState.ACTIVE);
		}
		
		protected function onMouseMove(e:MouseEvent):void
		{
			if (_drawable)
			{
				if (_lastDrawPos != null)
				{
					_lastDrawPos.copyFrom(_canvas.globalToLocal(new Point(e.stageX, e.stageY)));
					_canvas.doDraw(_lastDrawPos.x, _lastDrawPos.y);
				}
			}
		}
		
		protected function onMouseUp(e:MouseEvent):void
		{
			if (_drawable)
			{
				_lastDrawPos = null;
				_canvas.endDraw();
				_canvas.endErace();
			}
		}
		
		protected function onMouseDown(e:MouseEvent):void
		{
			if (_drawable)
			{
				var pos:Point = _canvas.globalToLocal(new Point(e.stageX, e.stageY));
				
				if (_enableDraw)
				{
					_lastDrawPos = pos;
					_canvas.startDraw(LineButton.lineWidth, ColorPickerButton.activeColor);
					_currentEditText = null;
				}
				
				if (_enableErace)
				{
					_canvas.startErace(50);
					_lastDrawPos = pos;
				}
				
				if ((_enableText) && (!_editText))
				{
					var newEditableText:TextField = new TextField();
					newEditableText.x = pos.x;
					newEditableText.y = pos.y;
					newEditableText.autoSize = TextFieldAutoSize.LEFT;
					newEditableText.border = false;
					newEditableText.background = false;
					newEditableText.type = TextFieldType.INPUT;
					newEditableText.multiline = true;
					_textFormat.color = ColorPickerButton.activeColor;
					newEditableText.defaultTextFormat = _textFormat;
					newEditableText.addEventListener(MouseEvent.MOUSE_DOWN, onTextFieldMouseDown);
					newEditableText.addEventListener(FocusEvent.FOCUS_OUT, onTextFieldFocusOut);
					_canvas.addChild(newEditableText);
					stage.focus = newEditableText;
					_currentEditText = newEditableText;
				}
			}
		}
		
		protected function initDraw(canvasWidth:int, canvasHeight:int):void
		{
			_canvas = new DrawCanvas(canvasWidth, canvasHeight);
			_canvas.x = -(_canvas.width / 2);
			addToScroll(_canvas);
			_drawable = true;
		}
		
		public function get drawable():Boolean
		{
			return _drawable;
		}
		
		public function set drawable(value:Boolean):void
		{
			if (_canvas != null)
			{
				if (value == false)
				{
					if (_drawable)
					{
						_scrollContainer.removeChild(_canvas);
					}
				}
				
				if (value == true)
				{
					if (!_drawable)
					{
						_scrollContainer.addChild(_canvas);
					}
				}
				
				_drawable = value;
			}
		}
		
		public function get canvas():DrawCanvas
		{
			return _canvas;
		}
		
		public function get enableDraw():Boolean
		{
			return _enableDraw;
		}
		
		public function set enableDraw(value:Boolean):void
		{
			_enableDraw = value;
		}
		
		public function get enableErace():Boolean
		{
			return _enableErace;
		}
		
		public function set enableErace(value:Boolean):void
		{
			_enableErace = value;
		}
		
		public function get enableText():Boolean
		{
			return _enableText;
		}
		
		public function set enableText(value:Boolean):void
		{
			if (value)
				enableDraw = false;
			_enableText = value;
		}
		
		override public function zoomIn():void
		{
			var newScale:Number = scrollContainer.scaleX + 0.1;
			if (newScale > 10)
				newScale = 10;
			TweenLite.to(scrollContainer, 0.3, {scaleX: newScale, scaleY: newScale});
		}
		
		override public function zoomOut():void
		{
			var newScale:Number = scrollContainer.scaleX - 0.1;
			if (newScale < 0.1)
				newScale = 0.1;
			TweenLite.to(scrollContainer, 0.3, {scaleX: newScale, scaleY: newScale});
		}
	
	}

}