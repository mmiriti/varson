package layout
{
	
	/**
	 * Главный контейнер
	 *
	 * @author Michael Miriti
	 */
	public class MainViewContainer extends LayoutElement
	{
		static public var I:MainViewContainer;
		
		private var _viewLeft:ViewContainer;
		private var _viewRight:ViewContainer;
		
		private var _viewFull:ViewContainer;
		private var _active:ViewContainer;
		
		private var _lastPutMethod:Function;
		
		private var _history:Vector.<HistoryFrame> = new Vector.<HistoryFrame>();
		private var _history_pointer:int = 0;
		
		public function MainViewContainer(containerWidth:Number = 1205)
		{
			super();
			I = this;
			_drawPlaceholder(0xe2e3e4, containerWidth, 680);
		}
		
		public function clearAll():void
		{
			if (_viewFull != null)
			{
				removeChild(_viewFull);
				_viewFull = null;
			}
			
			if (_viewLeft != null)
			{
				removeChild(_viewLeft);
				_viewLeft = null;
			}
			
			if (_viewRight != null)
			{
				removeChild(_viewRight);
				_viewRight = null;
			}
		}
		
		public function putAt(wnd:String, view:ViewContainer, addToHistory:Boolean = true):void
		{
			_active = view;
			
			switch (wnd)
			{
				case "full": 
					clearAll();					
					_viewFull = view;
					addChild(_viewFull);
					break;
				case "left": 
					if (_viewLeft != null)
					{
						removeChild(_viewLeft);
					}
					
					_viewLeft = view;
					
					if (view != null)
					{
						view.x = 0;
						addChild(view);
					}
					break;
				case "right": 
					if (_viewRight != null)
					{
						removeChild(_viewRight);
					}
					
					_viewRight = view;
					
					if (view != null)
					{
						view.x = 603;
						addChild(view);
					}
					
					break;
				default: 
					throw new Error("Wtf is <" + wnd + ">?!");
					return;
			}
			
			if (addToHistory)
			{
				_history[_history_pointer] = new HistoryFrame(_viewFull, _viewLeft, _viewRight);
				_history_pointer++;
			}
		}
		
		public function goHistory(go:int = -1):void
		{
			_history_pointer += go;
			if ((_history_pointer >= 0) && (_history_pointer < _history.length))
			{
				var hf:HistoryFrame = _history[_history_pointer];
				if (hf.full != null)
				{
					putFull(hf.full, false);
				}
				if (hf.left != null)
				{
					putAt("left", hf.left, false);
				}
				if (hf.right != null)
				{
					putAt("right", hf.right, false);
				}
			}
		}
		
		public function putFull(view:ViewContainer, addHistory:Boolean = true):void
		{
			putAt("full", view, addHistory);
			_lastPutMethod = putFull;
		}
		
		public function putLeft(view:ViewContainer, addHistory:Boolean = true):void
		{
			putAt("left", view, addHistory);
			
			_lastPutMethod = putLeft;
		}
		
		public function putRight(view:ViewContainer, addHistory:Boolean = true):void
		{
			putAt("right", view, addHistory);
			_lastPutMethod = putRight;
		}
		
		public function putLast(view:ViewContainer):void
		{
			_lastPutMethod.call(this, view);
		}
		
		public function getFull():ViewContainer
		{
			return _viewFull;
		}
		
		public function getLeft():ViewContainer
		{
			return _viewLeft;
		}
		
		public function getRight():ViewContainer
		{
			return _viewRight;
		}
		
		public function get active():ViewContainer
		{
			return _active;
		}
		
		public function set active(value:ViewContainer):void
		{
			_active = value;
		}
	
	}

}

import layout.ViewContainer;

class HistoryFrame
{
	private var _right:ViewContainer;
	private var _full:ViewContainer;
	private var _left:ViewContainer;
	
	public function HistoryFrame(hfull:ViewContainer, hleft:ViewContainer, hright:ViewContainer):void
	{
		_full = hfull;
		_left = hleft;
		_right = hright;
	}
	
	public function get right():ViewContainer
	{
		return _right;
	}
	
	public function get full():ViewContainer
	{
		return _full;
	}
	
	public function get left():ViewContainer
	{
		return _left;
	}
}