package layout
{
	import common.LayoutAlign;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	/**
	 * Выпадающий список с материалами
	 *
	 * @author Michael Miriti
	 */
	public class ContentDropDown extends LayoutElement
	{
		private var _opened:Boolean = false;
		private var _list:DropDownList;
		private var _viewContainer:ViewContainer;
		
		[Embed(source="../../assets/dropdown_button.png")]
		private static var _dropdownBitmap:Class;
		private var _dropDownButton:Bitmap;
		private var _text:TextField;
		private var _containerWidth:Number;
		private var _closeTimer:uint = 0;
		private var _dropDownFilter:*;
		private var openPlane:Sprite;
		
		public function ContentDropDown(containerWidth:Number, viewContainer:ViewContainer, dropDownFilter:* = null)
		{
			super();
			
			_dropDownFilter = dropDownFilter;
			_containerWidth = containerWidth;
			_viewContainer = viewContainer;
			_align = LayoutAlign.TOP;
			
			graphics.beginFill(0xffffff);
			graphics.lineStyle(1);
			graphics.drawRect(0, 0, containerWidth, 30);
			graphics.endFill();
			
			_text = new TextField();
			_text.selectable = false;
			_text.width = containerWidth;
			_text.autoSize = TextFieldAutoSize.LEFT;
			addChild(_text);
		}
		
		private function onListMouseOut(e:MouseEvent):void
		{
			_closeTimer = setTimeout(function():void
				{
					opened = false;
				}, 300);
		}
		
		public function setCaption(newCaption:String):void
		{
			_text.text = newCaption;
			_text.setTextFormat(Main.textFormatSmall);
		}
		
		override protected function onAddedToStage(e:Event):void
		{
			super.onAddedToStage(e);
			_list = new DropDownList(this, _dropDownFilter);
			_list.y = 30;
			_list.addEventListener(MouseEvent.MOUSE_OUT, onListMouseOut);
			_list.addEventListener(MouseEvent.MOUSE_OVER, onListMouseOver);
			
			_dropDownButton = new _dropdownBitmap() as Bitmap;
			_dropDownButton.x = _containerWidth - 30;
			addChild(_dropDownButton);
			
			openPlane = new Sprite();
			openPlane.graphics.beginFill(0x0);
			openPlane.graphics.drawRect(0, 0, width, 30);
			openPlane.graphics.endFill();
			openPlane.alpha = 0;
			addChild(openPlane);
			openPlane.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		private function onListMouseOver(e:MouseEvent):void
		{
			clearTimeout(_closeTimer);
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			opened = !opened;
		}
		
		public function get opened():Boolean
		{
			return _opened;
		}
		
		public function set opened(value:Boolean):void
		{
			_opened = value;
			if (_opened)
			{
				addChild(_list);
			}
			else
			{
				if (_list.parent)
				{
					removeChild(_list);
				}
			}
		}
		
		public function get viewContainer():ViewContainer 
		{
			return _viewContainer;
		}
	
	}

}
import com.greensock.easing.Elastic;
import com.greensock.TweenLite;
import common.Material;
import common.QuizMaterial;
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.filters.DropShadowFilter;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import layout.ContentDropDown;
import layout.views.AllMaterials;
import layout.views.MaterialView;
import layout.ViewContainer;
import layout.MainViewContainer;
import layout.ApplicationContainer;

class DropDownList extends Sprite
{
	private var _list:ContentDropDown;
	private var _stackY:Number = 0;
	private var _mask:Sprite;
	private var _container:Sprite = new Sprite();
	static private const MAX_HEIGHT:Number = 400;
	static private const SCROLL_STEP:Number = 30;
	
	[Embed(source="../../assets/scrollbar_button.png")]
	private static var _scrollButton:Class;
	private var _upButton:Sprite;
	private var _downButton:Sprite;
	private var _scrolling:Boolean = false;
	private var _scrollTween:TweenLite = null;
	
	public function DropDownList(list:ContentDropDown, _dropDownFilter:* = null):void
	{
		_list = list;
		addChild(_container);
		
		if ((_dropDownFilter == null) || (_dropDownFilter == "reset"))
		{
			var contentListItem:DropDownListItem = new DropDownListItem(_list, null, 0, AllMaterials.CONTENT_NAME, loadAllMaterials);
			addItem(contentListItem);
		}
		
		for (var i:int = 0; i < Main.materials.collection.length; i++)
		{
			if ((_dropDownFilter is MaterialView) && ((_dropDownFilter as MaterialView).material == Main.materials.collection[i]))
			{
				continue;
			}
			
			var currentMaterial:Material = Main.materials.collection[i];
			var sp:DropDownListItem = new DropDownListItem(_list, currentMaterial, 0, null, null, _dropDownFilter);
			addItem(sp);
			
			for (var j:int = 0; j < currentMaterial.childrenMaterials.length; j++)
			{
				addItem(new DropDownListItem(_list, currentMaterial.childrenMaterials[j], 1, null, null, _dropDownFilter));
			}
		}
	}
	
	public function addItem(item:DropDownListItem):void
	{
		item.y = _stackY;
		_stackY += item.height;
		_container.addChild(item);
		
		if ((height >= MAX_HEIGHT) && (!_scrolling))
		{
			initScroll();
		}
	}
	
	private function initScroll():void
	{
		_mask = new Sprite();
		_mask.graphics.beginFill(0x0);
		_mask.graphics.drawRect(0, 0, width, MAX_HEIGHT);
		_mask.graphics.endFill();
		addChild(_mask);
		mask = _mask;
		
		_upButton = new Sprite();
		_upButton.addChild(new _scrollButton());
		_upButton.x = width - _upButton.width - 2;
		addChild(_upButton);
		
		_downButton = new Sprite();
		var bmp:Bitmap = new _scrollButton();
		bmp.scaleY = -1;
		_downButton.addChild(bmp);
		_downButton.x = width - _downButton.width - 2;
		_downButton.y = MAX_HEIGHT;
		addChild(_downButton);
		
		_downButton.addEventListener(MouseEvent.MOUSE_DOWN, onScrollDown);
		_upButton.addEventListener(MouseEvent.MOUSE_DOWN, onScrollUp);
		
		_scrolling = true;
	}
	
	private function onScrollUp(e:MouseEvent):void
	{
		if (_scrollTween != null)
			_scrollTween.kill();
		
		_scrollTween = TweenLite.to(_container, 0.3, {y: _container.y + SCROLL_STEP, onComplete: function():void
			{
				if (_container.y > 0)
					_scrollTween = TweenLite.to(_container, 0.2, {y: 0});
			}});
	}
	
	private function onScrollDown(e:MouseEvent):void
	{
		if (_scrollTween != null)
			_scrollTween.kill();
		
		_scrollTween = TweenLite.to(_container, 0.3, {y: _container.y - SCROLL_STEP, onComplete: function():void
			{
				if (_container.y < MAX_HEIGHT - _container.height)
					_scrollTween = TweenLite.to(_container, 0.2, {y: MAX_HEIGHT - _container.height});
			}});
	}
	
	private function loadAllMaterials():void
	{
		var viewContainer:ViewContainer = (parent.parent as ViewContainer);
		viewContainer.loadView(new AllMaterials(viewContainer.getContainerWidth()));
	}
}

class DropDownListItem extends Sprite
{
	private var _material:Material;
	private var _callback:Function;
	private var _caption:String;
	private var _bg:Sprite = new Sprite();
	private var _filter:*;
	
	public function DropDownListItem(list:ContentDropDown, material:Material = null, level:int = 0, title:String = null, callback:Function = null, filter:* = null):void
	{
		_material = material;
		
		_filter = filter;
		
		_bg.graphics.beginFill(0xffffff);
		_bg.graphics.lineStyle(1, 0x0);
		_bg.graphics.drawRect(0, 0, list.width, 30);
		_bg.graphics.endFill();
		addChild(_bg);
		
		var text:TextField = new TextField();
		text.selectable = false;
		if (material != null)
		{
			_caption = "";
			
			for (var i:int = 0; i < level; i++)
			{
				_caption += "     ";
			}
			_caption += material.number + ". " + material.name;
		}
		else
		{
			if (title != null)
				_caption = title;
			else
				_caption = "";
		}
		
		text.text = _caption;
		text.autoSize = TextFieldAutoSize.LEFT;
		text.width = list.viewContainer.getContainerWidth();
		text.setTextFormat(Main.textFormatSmall);
		
		addChild(text);
		
		useHandCursor = true;
		
		_callback = callback;
		
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
	}
	
	private function onMouseOut(e:MouseEvent):void
	{
	
	}
	
	private function onMouseOver(e:MouseEvent):void
	{
	
	}
	
	/**
	 * Нажатие мыши
	 *
	 * @param	e
	 */
	private function onMouseDown(e:MouseEvent):void
	{
		if (_callback != null)
		{
			_callback.call(this);
		}
		else
		{
			if (_filter == "reset")
			{
				ApplicationContainer.I.resetMainViewContainer(new MainViewContainer());
				MainViewContainer.I.putFull(new ViewContainer().loadView(new MaterialView(1205, _material, !(_material is QuizMaterial))));
			}
			else
			{
				// TODO parent.parent.parent - ОЧЕЬ плохо!
				var viewContainer:ViewContainer = (parent.parent.parent.parent as ViewContainer);
				if (_material is QuizMaterial)
				{
					(_material as QuizMaterial).reset();
				}
				viewContainer.loadView(new MaterialView(viewContainer.getContainerWidth(), _material, !(_material is QuizMaterial)));
			}
			
			// TODO parent.parent.parent - ОЧЕЬ плохо!
			(parent.parent.parent as ContentDropDown).opened = false;
		}
	}
	
	public function get caption():String
	{
		return _caption;
	}
}