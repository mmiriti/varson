package layout
{
	import common.ClassFactory;
	import common.LayoutAlign;
	import flash.display.Bitmap;
	import layout.toolbars.ToolBarBottom;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Bottom extends LayoutElement
	{
		private var _toolbar:ToolBarBottom;
		
		public function Bottom()
		{
			super();
			
			var bitmapDataClass:Class = ClassFactory.getDesignClass("DesignFooter");
			var bottomBitmap:Bitmap = new Bitmap(new bitmapDataClass());
			addChild(bottomBitmap);
			
			_toolbar = new ToolBarBottom();
			_toolbar.x = 675;
			_toolbar.y = 8;
			addChild(_toolbar);
		}
	
	}

}