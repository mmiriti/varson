package layout
{
	import common.LayoutAlign;
	import layout.toolbars.ToolBarMain;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Left extends LayoutElement
	{
		
		public function Left()
		{
			super();
			
			graphics.beginFill(0xe2e3e4);
			graphics.drawRect(0, 0, 75, 680);
			graphics.endFill();
			
			addChild(new ToolBarMain());
		}
	
	}

}