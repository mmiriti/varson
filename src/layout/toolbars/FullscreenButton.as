package layout.toolbars
{
	import flash.display.StageDisplayState;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class FullscreenButton extends ToolBarButton
	{
		[Embed(source="../../../assets/toolbar_buttons/fullscreen_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../assets/toolbar_buttons/fullscreen_inactive.png")]
		private static var _inactive:Class;
		
		[Embed(source="../../../assets/toolbar_buttons/fullscreen_hover.png")]
		private static var _hover:Class;
		
		public function FullscreenButton()
		{
			super();
			_switch = true;
			scaleX = scaleY = 0.5;
		}
		
		override protected function onTriggered():void
		{
			super.onTriggered();
			if (state == ToolBarButtonState.INACTIVE)
				stage.displayState = StageDisplayState.NORMAL;
			if (state == ToolBarButtonState.ACTIVE)
				stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
		}
	
	}

}