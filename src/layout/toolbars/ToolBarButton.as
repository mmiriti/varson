package layout.toolbars
{
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import layout.LayoutElement;
	import layout.MainViewContainer;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ToolBarButton extends LayoutElement
	{
		protected var _switch:Boolean = false;
		protected var _depends:Vector.<Class> = new Vector.<Class>();
		protected var _viewTypes:Vector.<Class> = new Vector.<Class>();
		
		protected var _stateActive:Bitmap;
		protected var _stateInactive:Bitmap;
		protected var _stateHover:Bitmap;
		
		protected var _marginLeft:Number = 0;
		protected var _marginTop:Number = 0;
		protected var _marginRight:Number = 0;
		protected var _marginBottom:Number = 0;
		
		protected var _state:String = ToolBarButtonState.ACTIVE;
		
		private var _currentBitmap:Bitmap;
		
		public function ToolBarButton()
		{
			super();
			init();
			_setStateImage();
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		protected function activeTest():Boolean
		{
			return true;
		}
		
		private function _available():Boolean
		{
			return ((activeTest()) && (premitedView()));
		}
		
		private function _setCurrentBitmap(_bmp:Bitmap):void
		{
			if (_currentBitmap != null)
			{
				removeChild(_currentBitmap);
			}
			
			addChild(_bmp);
			
			_currentBitmap = _bmp;
		}
		
		protected function premitedView():Boolean
		{
			if ((MainViewContainer.I.active != null) && (_viewTypes.length != 0))
			{
				if (_viewTypes.indexOf(Class(getDefinitionByName(getQualifiedClassName(MainViewContainer.I.active.currentView)))) == -1)
				{
					return false;
				}
			}
			
			return true;
		}
		
		protected function _setStateImage():void
		{
			if (_state == ToolBarButtonState.ACTIVE)
				_setCurrentBitmap(_stateActive);
			
			if (_state == ToolBarButtonState.INACTIVE)
				_setCurrentBitmap(_stateInactive);
		}
		
		private function onMouseOut(e:MouseEvent):void
		{
			_setStateImage();
		}
		
		private function onMouseOver(e:MouseEvent):void
		{
			if (!_available())
				return;
			_setCurrentBitmap(_stateHover);
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			if (!_available())
				return;
			onTriggered();
		}
		
		protected function init():void
		{
		
		}
		
		private function dependenses():void
		{
			for (var i:int = 0; i < _depends.length; i++)
			{
				(parent as ToolBar).setState(_depends[i], ToolBarButtonState.INACTIVE);
			}
		}
		
		protected function onTriggered():void
		{
			if (_switch)
			{
				if (state == ToolBarButtonState.ACTIVE)
				{
					state = ToolBarButtonState.INACTIVE;
				}
				else
				{
					state = ToolBarButtonState.ACTIVE;
					dependenses();
				}
			}
			else
			{
				dependenses();
			}
		}
		
		public function get state():String
		{
			return _state;
		}
		
		public function set state(value:String):void
		{
			_state = value;
			
			_setStateImage();
		}
		
		public function get marginLeft():Number
		{
			return _marginLeft;
		}
		
		public function get marginTop():Number
		{
			return _marginTop;
		}
		
		public function get marginRight():Number
		{
			return _marginRight;
		}
		
		public function get marginBottom():Number
		{
			return _marginBottom;
		}
	
	}

}