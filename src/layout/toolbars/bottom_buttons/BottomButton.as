package layout.toolbars.bottom_buttons
{
	import layout.toolbars.ToolBarButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class BottomButton extends ToolBarButton
	{
		
		public function BottomButton()
		{
			super();
			scaleX = scaleY = 0.3;
		}
	
	}

}