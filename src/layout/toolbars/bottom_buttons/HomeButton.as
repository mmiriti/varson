package layout.toolbars.bottom_buttons
{
	import layout.ApplicationContainer;
	import layout.MainViewContainer;
	import layout.toolbars.main_buttons.SplitScreenButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.ViewContainer;
	import layout.views.AllMaterials;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class HomeButton extends BottomButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/home_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/home_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/home_inactive.png")]
		private static var _inactive:Class;
		
		public function HomeButton()
		{
			super();
			_marginRight = 100;
		}
		
		override protected function activeTest():Boolean
		{
			if (AssignmentsButton.Active)
				return false;
			return super.activeTest();
		}
		
		override protected function onTriggered():void
		{
			SplitScreenButton.instance.state = ToolBarButtonState.INACTIVE;
			SplitScreenButton.splitScreenActive = false;
			AdditionalInfoButton.I.state = ToolBarButtonState.INACTIVE;
			ApplicationContainer.I.resetMainViewContainer(new MainViewContainer(Main.W_WIDTH));
			MainViewContainer.I.putFull(new ViewContainer(Main.W_WIDTH).loadView(new AllMaterials(Main.W_WIDTH)));
			super.onTriggered();
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			
			super.init();
		}
	}

}