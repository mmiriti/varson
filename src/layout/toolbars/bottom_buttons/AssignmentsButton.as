package layout.toolbars.bottom_buttons
{
	import common.AllAssignments;
	import common.Material;
	import common.QuizMaterial;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import layout.ApplicationContainer;
	import layout.MainViewContainer;
	import layout.Popups;
	import layout.toolbars.main_buttons.SplitScreenButton;
	import layout.toolbars.ToolBarButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.ViewContainer;
	import layout.views.AllMaterials;
	import layout.views.MaterialView;
	import layout.views.View;
	import misc.HelpPager;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class AssignmentsButton extends BottomButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/assignments_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/assignments_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/assignments_inactive.png")]
		private static var _inactive:Class;
		
		public static var Active:Boolean = false;
		public static var Testing:View;
		public static var Instance:AssignmentsButton;
		
		private var allMenu:AllAssignments = null;
		
		public function AssignmentsButton()
		{
			super();
			Instance = this;
		}
		
		override protected function onTriggered():void
		{
			if (Active)
			{
				var viewMaterial:MaterialView = Testing.reProduce(1205) as MaterialView;
				viewMaterial.bestView();
				var viewContainer:ViewContainer = new ViewContainer().loadView(viewMaterial);
				
				MainViewContainer.I.clearAll();
				MainViewContainer.I.putFull(viewContainer);
				MainViewContainer.I.getFull().currentView.bestView();
				
				Active = false;
				state = ToolBarButtonState.INACTIVE;
			}
			else
			{
				if (MainViewContainer.I.active.currentView is AllMaterials)
				{
					showAll();
				}
				else
				{
					var view:MaterialView = MainViewContainer.I.active.currentView as MaterialView;
					
					if (view)
					{
						if (view.material.childrenMaterials.length > 0)
						{
							MainViewContainer.I.clearAll();
							var leftView:MaterialView = view.reProduce(600) as MaterialView;
							leftView.bestView();
							MainViewContainer.I.putLeft(new ViewContainer(600).loadView(leftView));
							MainViewContainer.I.getLeft().currentView.bestView();
							MainViewContainer.I.putRight(new ViewContainer(600).loadView(new MaterialView(600, (view.material.childrenMaterials[0] as QuizMaterial).reset(), false)));
							Active = true;
							Testing = view;
							state = ToolBarButtonState.ACTIVE;
						}
						else
						{
							showAll();
						}
					}
					else
					{
						showAll();
					}
				}
			}
			super.onTriggered();
		}
		
		private function showAll():void
		{
			if (allMenu == null)
			{
				allMenu = new AllAssignments();
			}
			
			Popups.instance.showPopup(allMenu);
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			super.init();
		}
	
	}

}