package layout.toolbars.bottom_buttons
{
	import common.AdditionalMaterial;
	import layout.Popups;
	import layout.toolbars.ToolBarButtonState;
	import misc.Help;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class HelpButton extends BottomButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/help_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/help_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/help_inactive.png")]
		private static var _inactive:Class;
		
		public function HelpButton()
		{
			super();
		
		}
		
		override protected function onTriggered():void
		{
			Popups.instance.showPopup(Help.getInstance());
			super.onTriggered();
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			
			super.init();
		}
	
	}

}