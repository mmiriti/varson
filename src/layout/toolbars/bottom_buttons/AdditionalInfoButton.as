package layout.toolbars.bottom_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class AdditionalInfoButton extends BottomButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/additional_info_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/additional_info_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/additional_info_inactive.png")]
		private static var _inactive:Class;
		
		public static var I:AdditionalInfoButton;
		
		public function AdditionalInfoButton()
		{
			super();
			I = this;
			_switch = true;
		}
		
		override protected function activeTest():Boolean
		{
			if (MainViewContainer.I.active.currentView is MaterialView)
			{
				if (!AssignmentsButton.Active)
				{
					var materialView:MaterialView = (MainViewContainer.I.active.currentView as MaterialView);
					return materialView.material.haveAdditionalInfo;
				}
				else
				{
					return false;
				}
			}
			else
				return false;
		}
		
		override public function get state():String
		{
			return super.state;
		}
		
		override public function set state(value:String):void
		{
			if (MainViewContainer.I)
			{
				if (MainViewContainer.I.active.currentView is MaterialView)
				{
					var materialView:MaterialView = (MainViewContainer.I.active.currentView as MaterialView);
					materialView.showAdditionalInfo = (value == ToolBarButtonState.ACTIVE);
				}
			}
			super.state = value;
		}
		
		override protected function init():void
		{
			super.init();
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			
			state = ToolBarButtonState.INACTIVE;
		}
	}

}