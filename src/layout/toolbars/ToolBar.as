package layout.toolbars
{
	import layout.LayoutElement;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ToolBar extends LayoutElement
	{
		private var _vertical:Boolean = false;
		
		protected var _buttons:Vector.<ToolBarButton> = new Vector.<ToolBarButton>()
		
		protected var _paddingLeft:Number = 5;
		protected var _paddingTop:Number = 5;
		protected var _spacingX:Number = 5;
		protected var _spacingY:Number = 5;
		
		private var _nextX:Number = 0;
		private var _nextY:Number = 0;
		
		public function ToolBar(tVertical:Boolean = true, tButtons:Vector.<ToolBarButton> = null)
		{
			super();
			_vertical = tVertical;
			
			if (tButtons != null)
			{
				for (var i:int = 0; i < tButtons.length; i++)
				{
					addButton(tButtons[i]);
				}
			}
		}
		
		public function addButton(nButton:ToolBarButton):void
		{
			nButton.x = _paddingLeft + _nextX + nButton.marginLeft;
			nButton.y = _paddingTop + _nextY + nButton.marginTop;
			addChild(nButton);
			
			_buttons.push(nButton);
			
			if (_vertical)
			{
				_nextY = nButton.y + nButton.height + _spacingY + nButton.marginBottom;
			}
			else
			{
				_nextX = nButton.x + nButton.width + _spacingX + nButton.marginRight;
			}
		}
		
		public function setState(buttonClass:Class, newState:String):void
		{
			for (var i:int = 0; i < _buttons.length; i++)
			{
				if (_buttons[i] is buttonClass)
				{
					_buttons[i].state = newState;
				}
			}
		}
	
	}

}