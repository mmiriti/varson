package layout.toolbars.main_buttons
{
	import common.QuizMaterial;
	import layout.MainViewContainer;
	import layout.toolbars.bottom_buttons.AssignmentsButton;
	import layout.toolbars.ToolBarButton;
	import layout.views.CleanBoard;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class MainToolBarButton extends ToolBarButton
	{
		public function MainToolBarButton()
		{
			super();
			scaleX = scaleY = 0.5;
			_viewTypes.push(MaterialView, CleanBoard);
		}
		
		override protected function activeTest():Boolean
		{
			if (AssignmentsButton.Active)
				return false;
			
			if (MainViewContainer.I.active.currentView is MaterialView)
			{
				if ((MainViewContainer.I.active.currentView as MaterialView).material is QuizMaterial)
					return false;
			}
			
			return super.activeTest();
		}
	}

}