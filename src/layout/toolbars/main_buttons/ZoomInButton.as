package layout.toolbars.main_buttons
{
	import flash.display.Bitmap;
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.DrawableView;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ZoomInButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/zoom_in_active.png")]
		private static var _zoom_in_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/zoom_in_inactive.png")]
		private static var _zoom_in_inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/zoom_in_hover.png")]
		private static var _zoom_in_hover:Class;
		
		public function ZoomInButton()
		{
			super();
		}
		
		override protected function activeTest():Boolean 
		{
			return true;
		}
		
		override protected function init():void
		{
			_stateActive = new _zoom_in_active() as Bitmap;
			_stateHover = new _zoom_in_hover() as Bitmap;		
			_stateInactive = new _zoom_in_inactive() as Bitmap;		
			state = ToolBarButtonState.INACTIVE;
		}
		
		override protected function onTriggered():void
		{
			if (MainViewContainer.I.active.currentView is DrawableView)
			{
				(MainViewContainer.I.active.currentView as DrawableView).zoomIn();
			}
		}
	}

}