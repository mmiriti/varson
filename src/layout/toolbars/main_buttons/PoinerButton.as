package layout.toolbars.main_buttons
{
	import layout.toolbars.ToolBarButtonState;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class PoinerButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/pointer_active.png")]
		private static var _poiner_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/pointer_inactive.png")]
		private static var _poiner_inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/pointer_hover.png")]
		private static var _poiner_hover:Class;
		
		private static var _enabled:Boolean = false;
		
		public function PoinerButton()
		{
			super();
			_depends.push(PencilButton, TextButton);
		}
		
		override protected function activeTest():Boolean 
		{
			return true;
		}
		
		override public function get state():String 
		{
			return super.state;
		}
		
		override public function set state(value:String):void 
		{
			super.state = value;
			_enabled = (state == ToolBarButtonState.ACTIVE);
		}
		
		override protected function init():void
		{
			_stateActive = new _poiner_active();
			_stateHover = new _poiner_hover();
			_stateInactive = new _poiner_inactive();
			
			state = ToolBarButtonState.INACTIVE;
			_switch = true;
		}
		
		static public function get enabled():Boolean
		{
			return _enabled;
		}
	
	}

}