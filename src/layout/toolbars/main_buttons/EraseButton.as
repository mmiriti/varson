package layout.toolbars.main_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.DrawableView;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class EraseButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/erase_active.png")]
		private static var _erase_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/erase_inactive.png")]
		private static var _erase_inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/erase_hover.png")]
		private static var _erase_hover:Class;
		
		public function EraseButton()
		{
			super();
			_switch = true;
			_depends.push(PoinerButton, PencilButton, TextButton, ColorPickerButton);
		}
		
		override protected function onTriggered():void
		{
			super.onTriggered();
		}
		
		override public function get state():String
		{
			return super.state;
		}
		
		override public function set state(value:String):void
		{
			super.state = value;
			if ((MainViewContainer.I) && (MainViewContainer.I.active.currentView is DrawableView))
			{
				if (value == ToolBarButtonState.ACTIVE)
					(MainViewContainer.I.active.currentView as DrawableView).enableErace = true;
				else
					(MainViewContainer.I.active.currentView as DrawableView).enableErace = false;
			}
		}
		
		override protected function init():void
		{
			_stateActive = new _erase_active();
			_stateHover = new _erase_hover();
			_stateInactive = new _erase_inactive();
			
			state = ToolBarButtonState.INACTIVE;
		}
	
	}

}