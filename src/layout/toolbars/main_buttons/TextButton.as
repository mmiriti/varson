package layout.toolbars.main_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.DrawableView;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class TextButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/text_active.png")]
		private static var _text_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/text_inactive.png")]
		private static var _text_inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/text_hover.png")]
		private static var _text_hover:Class;
		
		public function TextButton()
		{
			super();
			_switch = true;
			_depends.push(PencilButton, PoinerButton, EraseButton);
		}
		
		override public function get state():String
		{
			return super.state;
		}
		
		override public function set state(value:String):void
		{
			if ((MainViewContainer.I) && (MainViewContainer.I.active.currentView is DrawableView))
			{
				if (value == ToolBarButtonState.INACTIVE)
				{
					(MainViewContainer.I.active.currentView as DrawableView).enableText = false;
				}
				if (value == ToolBarButtonState.ACTIVE)
				{
					(MainViewContainer.I.active.currentView as DrawableView).enableText = true;
				}
			}
			
			super.state = value;
		}
		
		override protected function init():void
		{
			_stateActive = new _text_active();
			_stateHover = new _text_hover();
			_stateInactive = new _text_inactive();
			state = ToolBarButtonState.INACTIVE;
		}
	
	}

}