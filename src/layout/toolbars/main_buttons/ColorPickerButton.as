package layout.toolbars.main_buttons
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Point;
	import layout.ApplicationContainer;
	import layout.toolbars.ToolBarButtonState;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ColorPickerButton extends MainToolBarButton
	{
		public static var activeColor:uint = 0x000000;
		
		[Embed(source="../../../../assets/toolbar_buttons/color_choose_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/color_choose_inactive.png")]
		private static var _inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/color_choose_hover.png")]
		private static var _hover:Class;
		
		private var _picker:PickColor;
		private static var _changeColorHooks:Vector.<Function> = new Vector.<Function>();
		
		public function ColorPickerButton()
		{
			super();
			_switch = true;
		}
		
		public static function addChangeColorHook(hook:Function):void
		{
			_changeColorHooks.push(hook);
		}
		
		override protected function onAddedToStage(e:Event):void
		{
			super.onAddedToStage(e);
			var pos:Point = localToGlobal(new Point());
			_picker.x = 50;
			_picker.y = pos.y;
		}
		
		override protected function onTriggered():void
		{
			super.onTriggered();
			ApplicationContainer.I.topmostLayer.addChild(_picker);
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			_picker = new PickColor(onColorPick);
			
			super.init();
		}
		
		private function onColorPick(color:uint):void
		{
			ApplicationContainer.I.topmostLayer.removeChild(_picker);
			_stateActive = new Bitmap(new BitmapData(100, 100, false, color));		
			state = ToolBarButtonState.ACTIVE;
			activeColor = color;
			
			for (var i:int = 0; i < _changeColorHooks.length; i++)
			{
				_changeColorHooks[i].call(null, color);
			}
		}
	}

}
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;

class PickColor extends Sprite
{
	[Embed(source="../../../../assets/palette.png")]
	private static var _palette:Class;
	private var _paletteBitmap:Bitmap;
	private var _callback:Function;
	
	public function PickColor(callback:Function):void
	{
		_paletteBitmap = new _palette();
		addChild(_paletteBitmap);
		_callback = callback;
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
	}
	
	private function onMouseDown(e:MouseEvent):void
	{
		var color:uint = _paletteBitmap.bitmapData.getPixel(e.localX, e.localY);
		_callback.call(null, color);
	}
}