package layout.toolbars.main_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.bottom_buttons.AdditionalInfoButton;
	import layout.toolbars.bottom_buttons.AssignmentsButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.ViewContainer;
	import layout.views.AllMaterials;
	import layout.views.MaterialView;
	import layout.views.View;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class SplitScreenButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/split_screen_active.png")]
		private static var _split_screen_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/split_screen_hover.png")]
		private static var _split_screen_hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/split_screen_inactive.png")]
		private static var _split_screen_inactive:Class;
		
		static private var _splitScreenActive:Boolean = false;
		static private var _instance:SplitScreenButton;
		
		public function SplitScreenButton()
		{
			super();
			_instance = this;
			_depends.push(TextButton, EraseButton, ColorPickerButton, LineButton, PencilButton);
		}
		
		override protected function init():void
		{
			_stateActive = new _split_screen_active();
			_stateHover = new _split_screen_hover();
			_stateInactive = new _split_screen_inactive();
			state = ToolBarButtonState.INACTIVE;
		}
		
		override protected function onTriggered():void
		{
			var view:View;
			
			if (_splitScreenActive)
			{
				view = MainViewContainer.I.active.currentView.reProduce(1205);
				MainViewContainer.I.clearAll();
				MainViewContainer.I.putFull(new ViewContainer(1205).loadView(view));
				MainViewContainer.I.getFull().currentView.bestView();
				_splitScreenActive = false;
				
				state = ToolBarButtonState.INACTIVE;
			}
			else
			{
				AdditionalInfoButton.I.state = ToolBarButtonState.INACTIVE;
				
				view = MainViewContainer.I.active.currentView.reProduce(600);
				
				MainViewContainer.I.clearAll();
				MainViewContainer.I.putLeft(new ViewContainer(600).loadView(view));
				MainViewContainer.I.putRight(new ViewContainer(600, view).loadView(new View(600)));
				_splitScreenActive = true;
				
				state = ToolBarButtonState.ACTIVE;
			}
		}
		
		static public function get splitScreenActive():Boolean
		{
			return _splitScreenActive;
		}
		
		static public function set splitScreenActive(value:Boolean):void
		{
			_splitScreenActive = value;
		}
		
		static public function get instance():SplitScreenButton
		{
			return _instance;
		}
	
	}

}