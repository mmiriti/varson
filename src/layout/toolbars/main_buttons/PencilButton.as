package layout.toolbars.main_buttons
{
	import flash.events.Event;
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.DrawableView;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class PencilButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/pencil_active.png")]
		private static var _pencil_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/pencil_inactive.png")]
		private static var _pencil_inactive:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/pencil_hover.png")]
		private static var _pencil_hover:Class;
		
		private static var _enabled:Boolean;
		
		public function PencilButton()
		{
			super();
			_switch = true;
			_depends.push(PoinerButton, TextButton, EraseButton);
		}
		
		override protected function init():void
		{
			_stateActive = new _pencil_active();
			_stateHover = new _pencil_hover();
			_stateInactive = new _pencil_inactive();
			
			state = ToolBarButtonState.INACTIVE;
		}
		
		override public function set state(val:String):void
		{
			if ((MainViewContainer.I) && (MainViewContainer.I.active.currentView is DrawableView))
			{
				if (val == ToolBarButtonState.ACTIVE)
					(MainViewContainer.I.active.currentView as DrawableView).enableDraw = true;
				else
					(MainViewContainer.I.active.currentView as DrawableView).enableDraw = false;
			}
			super.state = val;
			_enabled = (val == ToolBarButtonState.ACTIVE);
		}
		
		static public function get enabled():Boolean
		{
			return _enabled;
		}
	
	}

}