package layout.toolbars.main_buttons
{
	import flash.events.Event;
	import flash.geom.Point;
	import layout.ApplicationContainer;
	import layout.toolbars.ToolBarButtonState;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class LineButton extends MainToolBarButton
	{
		
		[Embed(source="../../../../assets/toolbar_buttons/line_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/line_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/line_inactive.png")]
		private static var _inactive:Class;
		
		private var _toolbar:LinesToolbar;
		
		public static var lineWidth:int = 1;
		
		public function LineButton()
		{
			super();
			_toolbar = new LinesToolbar(this);
		}
		
		public function setWidth(wd:int):void
		{
			lineWidth = wd;
		}
		
		override protected function onAddedToStage(e:Event):void
		{
			super.onAddedToStage(e);
			var p:Point = localToGlobal(new Point());
			_toolbar.x = 50;
			_toolbar.y = p.y;
		}
		
		override protected function onTriggered():void
		{
			super.onTriggered();
			_toolbar.startTimer();
			ApplicationContainer.I.topmostLayer.addChild(_toolbar);
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			super.init();
		}
	}

}
import flash.events.Event;
import flash.utils.clearTimeout;
import flash.utils.setTimeout;
import layout.toolbars.main_buttons.LineButton;
import flash.display.Bitmap;
import layout.toolbars.ToolBar;
import layout.toolbars.main_buttons.MainToolBarButton;

class LineToolbarButton extends MainToolBarButton
{
	private var _lineButton:LineButton;
	protected var _lineWidth:int = 1;
	
	public function LineToolbarButton(lb:LineButton):void
	{
		_lineButton = lb;
		super();
	}
	
	override protected function onTriggered():void
	{
		super.onTriggered();
		_lineButton.setWidth(_lineWidth);
		parent.parent.removeChild(parent);
	}
}

class LineButton1 extends LineToolbarButton
{
	[Embed(source="../../../../assets/toolbar_buttons/lines/1x.png")]
	private static var _image:Class;
	
	public function LineButton1(lb:LineButton):void
	{
		super(lb);
	}
	
	override protected function init():void
	{
		_lineWidth = 1;
		_stateActive = new _image();
		_stateHover = _stateActive;
		_stateInactive = _stateActive;
		super.init();
	}
}

class LineButton2 extends LineToolbarButton
{
	[Embed(source="../../../../assets/toolbar_buttons/lines/2x.png")]
	private static var _image:Class;
	
	public function LineButton2(lb:LineButton):void
	{
		super(lb);
	}
	
	override protected function init():void
	{
		_lineWidth = 2;
		_stateActive = new _image();
		_stateHover = _stateActive;
		_stateInactive = _stateActive;
		super.init();
	}
}

class LineButton3 extends LineToolbarButton
{
	[Embed(source="../../../../assets/toolbar_buttons/lines/4x.png")]
	private static var _image:Class;
	
	public function LineButton3(lb:LineButton):void
	{
		super(lb);
	}
	
	override protected function init():void
	{
		_lineWidth = 4;
		_stateActive = new _image();
		_stateHover = _stateActive;
		_stateInactive = _stateActive;
		super.init();
	}
}

class LinesToolbar extends ToolBar
{
	private var closeTime:uint;
	
	public function LinesToolbar(lineButton:LineButton):void
	{
		super(true);
		_spacingX = _spacingY = _paddingLeft = _paddingTop = 0;
		addButton(new LineButton1(lineButton));
		addButton(new LineButton2(lineButton));
		addButton(new LineButton3(lineButton));
	}
	
	public function startTimer():void
	{
		clearTimeout(closeTime);
		closeTime = setTimeout(close, 2000);
	}
	
	public function resetTimer():void
	{
		clearTimeout(closeTime);
	}
	
	private function close():void
	{
		if (parent != null)
			parent.removeChild(this);
	}
}