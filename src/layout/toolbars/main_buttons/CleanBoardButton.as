package layout.toolbars.main_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButtonState;
	import layout.ViewContainer;
	import layout.views.CleanBoard;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class CleanBoardButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/cleanboard_active.png")]
		private static var _active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/cleanboard_hover.png")]
		private static var _hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/cleanboard_inactive.png")]
		private static var _inactive:Class;
		
		public function CleanBoardButton()
		{
			super();
			_depends.push(PoinerButton, TextButton, EraseButton, PencilButton);
		}
		
		override protected function onTriggered():void 
		{
			MainViewContainer.I.clearAll();
			MainViewContainer.I.putFull(new ViewContainer(1205).loadView(new CleanBoard()));
			super.onTriggered();
		}
		
		override protected function init():void
		{
			_stateActive = new _active();
			_stateHover = new _hover();
			_stateInactive = new _inactive();
			state = ToolBarButtonState.INACTIVE;
			super.init();
		}
	
	}

}