package layout.toolbars.main_buttons
{
	import layout.MainViewContainer;
	import layout.toolbars.ToolBarButton;
	import layout.toolbars.ToolBarButtonState;
	import layout.views.DrawableView;
	import layout.views.MaterialView;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ZoomOutButton extends MainToolBarButton
	{
		[Embed(source="../../../../assets/toolbar_buttons/zoom_out_active.png")]
		private static var _zoom_out_active:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/zoom_out_hover.png")]
		private static var _zoom_out_hover:Class;
		
		[Embed(source="../../../../assets/toolbar_buttons/zoom_out_inactive.png")]
		private static var _zoom_out_inactive:Class;
		
		public function ZoomOutButton()
		{
			super();		
		}
		
		override protected function activeTest():Boolean 
		{
			return true;
		}
		
		override protected function init():void
		{
			_stateActive = new _zoom_out_active();
			_stateHover = new _zoom_out_hover();
			_stateInactive = new _zoom_out_inactive();
			
			state = ToolBarButtonState.INACTIVE;
		}
		
		override protected function onTriggered():void
		{
			if (MainViewContainer.I.active.currentView is DrawableView)
			{
				(MainViewContainer.I.active.currentView as DrawableView).zoomOut();
			}
		}
	}

}