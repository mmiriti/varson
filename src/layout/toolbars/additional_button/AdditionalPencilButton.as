package layout.toolbars.additional_button
{
	import layout.toolbars.main_buttons.PencilButton;
	import layout.toolbars.ToolBarButtonState;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class AdditionalPencilButton extends PencilButton
	{
		private static var _enabled:Boolean = false;
		
		public function AdditionalPencilButton()
		{
			super();
			
			_depends = new Vector.<Class>();
			_depends.push(AdditionalEracer);
		}
		
		override public function set state(val:String):void
		{
			_state = val;
			_setStateImage();
			_enabled = (val == ToolBarButtonState.ACTIVE);
		}
		
		static public function get enabled():Boolean
		{
			return _enabled;
		}
	
	}

}