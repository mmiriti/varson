package layout.toolbars
{
	import layout.toolbars.bottom_buttons.AdditionalInfoButton;
	import layout.toolbars.bottom_buttons.AssignmentsButton;
	import layout.toolbars.bottom_buttons.HelpButton;
	import layout.toolbars.bottom_buttons.HomeButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ToolBarBottom extends ToolBar
	{
		
		public function ToolBarBottom()
		{
			super(false);
			addButton(new HomeButton());
			addButton(new AssignmentsButton());
			addButton(new AdditionalInfoButton());
			addButton(new HelpButton());
		}
	
	}

}