package layout.toolbars
{
	import layout.toolbars.additional_button.AdditionalEracer;
	import layout.toolbars.additional_button.AdditionalPencilButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ToolbarAdditionInfo extends ToolBar
	{
		
		public function ToolbarAdditionInfo()
		{
			super(true);
			_paddingLeft = 10;
			_paddingTop = 10;
			addButton(new AdditionalPencilButton());
			addButton(new AdditionalEracer());
		}
	
	}

}