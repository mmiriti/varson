package layout.toolbars
{
	import layout.toolbars.main_buttons.CleanBoardButton;
	import layout.toolbars.main_buttons.ColorPickerButton;
	import layout.toolbars.main_buttons.EraseButton;
	import layout.toolbars.main_buttons.LineButton;
	import layout.toolbars.main_buttons.PencilButton;
	import layout.toolbars.main_buttons.PoinerButton;
	import layout.toolbars.main_buttons.SplitScreenButton;
	import layout.toolbars.main_buttons.TextButton;
	import layout.toolbars.main_buttons.ZoomInButton;
	import layout.toolbars.main_buttons.ZoomOutButton;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class ToolBarMain extends ToolBar
	{
		private static var _I:ToolBarMain;
		
		public function ToolBarMain()
		{
			super(true);
			_I = this;
			_paddingLeft = 10;
			_paddingTop = 10;
			
			addButton(new PoinerButton());
			addButton(new ZoomInButton());
			addButton(new ZoomOutButton());
			addButton(new PencilButton());
			addButton(new ColorPickerButton());
			addButton(new LineButton());
			addButton(new EraseButton());
			addButton(new TextButton());
			addButton(new SplitScreenButton());
			addButton(new CleanBoardButton());
		}
		
		static public function get I():ToolBarMain
		{
			return _I;
		}
	
	}

}