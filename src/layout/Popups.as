package layout
{
	import com.greensock.TweenLite;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.errors.IllegalOperationError;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Michael Miriti
	 */
	public class Popups extends Sprite
	{
		private static var _instance:Popups;
		
		[Embed(source="../../assets/popup-closeButton.png")]
		private static var _closeButtonClass:Class;
		private var closeButton:Bitmap = new _closeButtonClass() as Bitmap;
		
		private var _lastContent:DisplayObject = null;
		private var _background:Sprite;
		private var _shown:Boolean = false;
		
		public function Popups()
		{
			super();
			if (_instance != null)
			{
				throw new IllegalOperationError("Use singletone");
			}
			
			_background = new Sprite();
			_background.graphics.beginFill(0x0, 0.8);
			_background.graphics.drawRect(0, 0, Main.W_WIDTH, Main.W_HEIGHT);
			_background.graphics.endFill();
			_background.addEventListener(MouseEvent.MOUSE_DOWN, onBackgroundMouseDown);
			addChild(_background);
		}
		
		private function onBackgroundMouseDown(e:MouseEvent):void
		{
			hidePopup();
		}
		
		/**
		 * Скрыть попап
		 *
		 */
		public function hidePopup(callback:Function = null):void
		{
			if (_shown)
			{
				var thisDisplayObject:DisplayObject = this;
				TweenLite.to(this, 0.5, {alpha: 0, onComplete: function():void
					{
						_shown = false;
						removeChild(closeButton);
						ApplicationContainer.I.removeChild(thisDisplayObject);
						if (callback != null)
							callback.call();
					}});
			}
		}
		
		/**
		 * Показать попап
		 *
		 * @param	content
		 */
		public function showPopup(content:DisplayObject = null, callback:Function = null, autoBackground:Boolean = false):void
		{
			if (!_shown)
			{
				if (content == null)
				{
					content = _lastContent;
				}
				else
				{
					if (_lastContent != null)
						removeChild(_lastContent);
					_lastContent = content;
				}
				
				if (content != null)
				{
					content.x = (width - content.width) / 2;
					content.y = (height - content.height) / 2;
					
					addChild(content);
					
					closeButton.x = content.x + content.width - closeButton.width / 2;
					closeButton.y = content.y - closeButton.height / 2;
					addChild(closeButton);
				}
				
				alpha = 0;
				ApplicationContainer.I.addChild(this);
				TweenLite.to(this, 0.5, {alpha: 1, onComplete: function():void
					{
						if (callback != null)
							callback.call();
					}});
				_shown = true;
			}
		}
		
		static public function get instance():Popups
		{
			if (_instance == null)
				_instance = new Popups();
			return _instance;
		}
	
	}
}