package layout
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import layout.views.View;
	
	/**
	 * Контейнер с выпадающим списком для просмотра материалов
	 *
	 * @author Michael Miriti
	 */
	public class ViewContainer extends LayoutElement
	{
		private var _container:Sprite = new Sprite();
		private var _contentDropDown:ContentDropDown;
		private var _currentView:View;
		private var _containerWidth:int;
		
		public function ViewContainer(containerWidth:int = 1205, dropDownFilter:* = null)
		{
			_containerWidth = containerWidth;
			super();
			_drawPlaceholder(0xffffff, containerWidth, 680);
			
			_contentDropDown = new ContentDropDown(containerWidth, this, dropDownFilter);
			_contentDropDown.x = _contentDropDown.y = 0;
			
			addChild(_container);
			if (dropDownFilter !== false){
				addChild(_contentDropDown);
			}
			
			_container.x = 0;
			_container.y = _contentDropDown.y + 30;
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			(parent as MainViewContainer).active = this;
		}
		
		public function getContainerWidth():int
		{
			return _containerWidth;
		}
		
		public function loadView(view:View):ViewContainer
		{
			if (_currentView != null)
			{
				_container.removeChild(_currentView);
			}
			_currentView = view;
			_currentView.onView(this);
			_container.addChild(_currentView);
			
			return this;
		}
		
		public function get contentDropDown():ContentDropDown
		{
			return _contentDropDown;
		}
		
		public function get currentView():View
		{
			return _currentView;
		}
	
	}

}