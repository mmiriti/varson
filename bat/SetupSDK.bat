:user_configuration

:onx86
set FLEX_SDK=C:\Program Files\FlashDevelop\Tools\flexsdk
goto validationx86

:onx64
set FLEX_SDK=C:\Program Files (x86)\FlashDevelop\Tools\flexsdk
goto validationx64


:validationx86
if not exist "%FLEX_SDK%" goto onx64
goto succeed

:validationx64
if not exist "%FLEX_SDK%" goto flexsdk
goto succeed


:flexsdk
echo.
echo ERROR: incorrect path to Flex SDK in 'bat\SetupSDK.bat'
echo.
echo %FLEX_SDK%
echo.
if %PAUSE_ERRORS%==1 pause
exit

:succeed
set PATH=%PATH%;%FLEX_SDK%\bin

